'use strict';

var fs = require('fs');
const path = require('path');
var Promise = require("bluebird");
const { exec } = require('child_process');
var sizeOf = require('image-size');
var debug = 1;

module.exports = {
    checkDir: function (dir) {
        let dir_exist = fs.existsSync(dir);
        if (!dir_exist) {
            let mkdir = fs.mkdirSync(dir);
            if (mkdir) {
                console.log("mkdir", mkdir);
            }
        } else {
            console.log("dir exists");
        }
    },
    compile: function (map, cb) {
        console.log("cm map", map);
        let map_dir = path.join(__dirname, '..', '..', 'client', 'maps', map.id + '') + '/';
        this.checkDir(map_dir);
        let tiles_dirs = this.getTilesDirs(map.min_level, map.max_level);
        if (debug) {
            console.log("tiles dirs", tiles_dirs);
        }
        let self = this;
        this.cleanTilesDirs(tiles_dirs, map_dir).then(function() {
            if (debug) {
                console.log("clean tiles dirs finish");
            }
            return self.checkTilesDirs(tiles_dirs, map_dir).then(function() {
                let map_name = 'm' + map.id;
                let sizes = self.getSizes(map.min_level, map.max_level);
                let ext = 'png';
                if (debug) {
                    console.log("map_name", map_name);
                    console.log("sizes", sizes);
                    console.log("check tiles dirs finish");
                }
                return self.cleanResizedCopies(map_dir, map_name, sizes, ext).then(function() {
                    if (debug) {
                        console.log("clean resized copies finish");
                    }
                    return self.getResizedCopies(map_dir, map_name, sizes, ext).then(function(sizes_files) {
                        if (debug) {
                            console.log("sizes files", sizes_files);
                            console.log("get resized copies finish");
                        }
                        let crop_size = map.tile_size;
                        return self.getTiles(sizes_files, map_dir, tiles_dirs, crop_size, ext).then(function() {
                            if (debug) {
                                console.log("get tiles finish");
                            }
                            return self.convertTilesNames(sizes_files, tiles_dirs, map_dir, crop_size, ext).then(function() {
                                if (debug) {
                                    console.log("convert tiles finish");
                                }
                            });
                        });
                    });
                });
            });
        }).then(function(err, result) {
            return cb(err, result);
        });
    },
    getSizes: function(min, max) {
        let levels = max - min;
        levels = levels > 0 ? levels : 1;
        if (levels == 1) {
            return ['100%'];
        }
        let sizes = Array.apply(null, Array(levels + 1)).map(function (_, i) {
            if(!i) {
                return '100%';
            }
            return (100 / Math.pow(2, i)) + '%';
        });
        return sizes;
    },
    getTilesDirs: function (min, max) {
        let levels = max - min;
        levels = levels > 0 ? levels : 1;
        if(levels == 1) {
            return [max];
        }
        let dirs = Array.apply(null, Array(levels + 1)).map(function (_, i) { return min + i; });
        return dirs.reverse();
    },
    checkTilesDirs: function(tiles_dirs, map_dir) {
        return Promise.map(tiles_dirs, function(tile_dir) {
            return new Promise(function (resolve, reject) {
                let dir = map_dir + tile_dir;
                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, function(err) {
                        if (err) {
                            console.log("cant mk dir", dir, err);
                        }
                        resolve(true);
                    });
                } else {
                    resolve(true);
                }
            });
        });
    },
    cleanTilesDirs: function (tiles_dirs, map_dir) {
        return Promise.map(tiles_dirs, function (tile_dir) {
            return new Promise(function (resolve, reject) {
                let dir = map_dir + tile_dir;
                if (fs.existsSync(dir)) {
                    return exec('rm -rf ' + dir + '/*', function(stdout, stderr) {
                        console.log(`stdout: ${stdout}`);
                        console.log(`stderr: ${stderr}`);
                        resolve(true);
                    });
                } else {
                    resolve(true);
                }
            });
        });
    },
    cleanResizedCopies: function (map_dir, map_name, sizes, ext) {
        return Promise.map(sizes, function (size) {
            return new Promise(function (resolve, reject) {
                let clean_size = size.replace('%', '').replace(',', '').replace('.', '');
                let size_map = map_dir + map_name + '_' + clean_size + '.' + ext;
                if (fs.existsSync(size_map)) {
                    return fs.unlink(size_map, function (err) {
                        if(err) {
                            console.log("cant rm size map", err);
                        }
                        console.log("rm size map", size_map);
                        resolve(true);
                    });
                } else {
                    resolve(true);
                }
            });
        });
    },
    getResizedCopies: function (map_dir, map_name, sizes, ext) {
        let sizes_files = [];
        return Promise.each(sizes, function (size) {
            return new Promise(function (resolve, reject) {
                let clean_size = size.replace('%', '').replace(',', '').replace('.', '');
                let source_map;
                if (debug) {
                    console.log("clean size", clean_size);
                }
                if (clean_size == 100) {
                    source_map = map_dir + map_name + '.' + ext;
                } else {
                    source_map = map_dir + map_name + '_100.' + ext;
                }
                let size_map = map_dir + map_name + '_' + clean_size + '.' + ext;
                sizes_files.push(size_map);
                let cmd = 'convert ' + source_map + ' -resize ' + size + ' ' + size_map;
                if(debug) {
                    console.log("resize cmd", cmd);
                }
                exec(cmd, function(err, stdout, stderr) {
                    if (err) {
                        console.log('cant resize', err);
                    }
                    console.log(`stdout: ${stdout}`);
                    console.log(`stderr: ${stderr}`);
                    resolve(size_map);
                });
            });
        }).then(function() {
            return sizes_files;
        });
    },
    getTiles: function (sizes_files, map_dir, tiles_dirs, crop_size, ext) {
        return Promise.map(sizes_files, function (size_file, index) {
            return new Promise(function (resolve, reject) {
                let cmd = 'convert -crop ' + crop_size + 'x' + crop_size + ' ' + size_file + '  ' + map_dir + tiles_dirs[index] + '/%d.png';
                exec(cmd, (err, stdout, stderr) => {
                    if (err) {
                        console.log("err", err);
                    }
                    console.log("sf i", size_file, index);
                    console.log(`stdout: ${stdout}`);
                    console.log(`stderr: ${stderr}`);
                    resolve(true);
                });
            });
        });
    },
    convertTilesNames: function (sizes_files, tiles_dirs, map_dir, crop_size, ext) {
        return Promise.map(sizes_files, function (size_file, index) {
            return new Promise(function (resolve, reject) {
                let tiles_dir = map_dir + tiles_dirs[index] + '/';
                fs.readdir(tiles_dir, (err, files) => { 
                    let tiles_count = files.length;
                    var dimensions = sizeOf(size_file);
                    let tiles_count_on_row = Math.ceil(dimensions.width / crop_size);
                    let tiles = Array.apply(null, Array(tiles_count + 1)).map(function (_, i) { return i; });
                    Promise.map(tiles, function(i) {
                        return new Promise(function (resolve, reject) {
                            let old_filename = tiles_dir + i + '.' +ext;
                            if (fs.existsSync(old_filename)) {
                                let x = parseInt(i / tiles_count_on_row);
                                let y = i % tiles_count_on_row;
                                let filename = y + 'x' + x;
                                let new_filename = tiles_dir + filename + '.' + ext;
                                fs.rename(old_filename, new_filename, function(err) {
                                    if (err) {
                                        console.log('rename err:', err);
                                    } else {
                                        console.log('rename suc', old_filename, new_filename);
                                    }
                                    resolve(true);
                                });
                            } else {
                                resolve(true);
                            }
                        });
                    }).then(function() {
                        resolve(true);
                    });
                });
            });
        });
    }
};
