'use strict';

var Promise = require("bluebird");
var _ = require("lodash");
var astar = require("./astar");
var debug = 1;
var path_algo_type = 'diagonal';
var path_cut_corner = true;

module.exports = {
    find: function (map, objects, from_id, to_id, cb) {
        if (debug) {
            console.log("map", map);
            console.log("objects", objects);
            console.log("from_id", from_id);
            console.log("to_id", to_id);
        }
        let map_spec = this.getMapSpec(map);
        if (debug) {
            console.log("map_spec", map_spec);
        }
        let grid_objects = this.getGridObjects(objects, map_spec);
        if (debug) {
         //   console.log("grid_objects", grid_objects);
        }
        let map_arr = this.buildDigitMap(map_spec, grid_objects);
        if (debug) {
         //   console.log("map_arr", map_arr);
        }
        let from_obj = this.findObject(map_spec, objects, from_id);
        if (debug) {
         //   console.log("from_obj", from_obj);
        }
        let to_obj = this.findObject(map_spec, objects, to_id);
        if (debug) {
        //    console.log("to_obj", to_obj);
        }
        let from_empty_around_points = this.findEmptyAround(map_arr, from_obj);
        if (debug) {
          //  console.log("from_empty_around_points", from_empty_around_points);
        }
        let to_empty_around_points = this.findEmptyAround(map_arr, to_obj);
        if (debug) {
          //  console.log("to_empty_around_points", to_empty_around_points);
        }
        let start_point = this.chooseTargetPoint(from_empty_around_points);
        if (debug) {
         //   console.log("start_point", start_point);
        }
        let end_point = this.chooseTargetPoint(to_empty_around_points);
        if (debug) {
          //  console.log("end_point", end_point);
        }
        map_arr = this.addTargetPoints(map_arr, start_point, end_point);
        if (debug) {
         //   console.log("full_map", map_arr);
        }
        let path = this.findPath(map_arr);
        if (debug) {
            console.log("path", path);
        }
        let converted_path = this.convertPath(map_spec, path);
        if (debug) {
            console.log("converted_path", converted_path);
        }
        if (debug) {
            map_arr = this.drawPath(map_arr, path);
          //  console.log("map_with_path", map_arr);
        }
        cb(null, converted_path);
    },
    getMapSpec: function (map) {
        let map_spec = {};
        map_spec['cell_size'] = map.cell_size;
        map_spec['width'] = this.cellRound(map.width, map_spec['cell_size']);
        map_spec['height'] = this.cellRound(map.height, map_spec['cell_size']);
        map_spec['zero_point_x'] = this.cellRound(map.zero_point_x, map_spec['cell_size']);
        map_spec['zero_point_y'] = this.cellRound(map.zero_point_y, map_spec['cell_size']);
        map_spec['square_size'] = map_spec['width'] > map_spec['height'] ? map_spec['width'] : map_spec['height'];
        map_spec['grid_size'] = map_spec['square_size'] / map_spec['cell_size'];
        map_spec['offset_w'] = map.width < map_spec['width'] ? 1 : 0;
        map_spec['offset_h'] = map.height < map_spec['height'] ? 1 : 0;
        return map_spec;
    },
    cellRound: function (size, cell_size) {
        let new_size;
        let koef = 1;
        if (size < 0) {
            koef = -1;
        } else {
            koef = 1;
        }
        new_size = Math.ceil(Math.abs(size) / cell_size) * cell_size;
        return koef * new_size;
    },
    getGridObjects: function (objects, map_spec) {
        let grid_points = [];
        let self = this;
        _.each(objects, function (value) {
            let object = self.getObjectSpec(value, map_spec);
            let x = object['grid_new_x'];
            let y = object['grid_new_y'];
            let w = object['w'];
            let h = object['h'];
            for (var j = 0; j < h; j++) {
                if (grid_points[y + j] == undefined) {
                    grid_points[y + j] = [];
                }
                for (var i = 0; i < w; i++) {
                    grid_points[y + j][x + i] = 1;
                }
            }
        });
        return grid_points;
    },
    getObjectSpec: function (object, map_spec) {
        if (object.size == undefined) {
            return {};
        }
        var pack = {};
        pack['w'] = object.size.split(" ")[0] / map_spec['cell_size'];
        pack['h'] = object.size.split(" ")[1] / map_spec['cell_size'];
        pack['orig_x'] = parseInt(object.pos.split(" ")[0]);
        pack['orig_y'] = parseInt(object.pos.split(" ")[1]);
        pack['new_x'] = pack['orig_x'] - map_spec['zero_point_x'];
        pack['new_y'] = pack['orig_y'] - map_spec['zero_point_y'];
        pack['grid_new_x'] = pack['new_x'] / map_spec['cell_size'];
        pack['grid_new_y'] = pack['new_y'] / map_spec['cell_size'];
        return pack;
    },
    buildDigitMap: function (map_spec, grid_points) {
        let map_arr;
        map_arr = [];
        for (var x = 0; x < map_spec['grid_size']; x++) {
            map_arr[x] = [];
            for (var y = 0; y < map_spec['grid_size']; y++) {
                if (grid_points[x] != undefined && grid_points[x][y] != undefined) {
                    map_arr[x][y] = 1;
                } else {
                    map_arr[x][y] = 0;
                }
            }
        }
        return map_arr;
    },
    findObject: function (map_spec, objects, obj_id) {
        let obj = [];
        let self = this;
        _.each(objects, function(value){
            if(value.id == obj_id) {
                let object = self.getObjectSpec(value, map_spec);
                let x = object['grid_new_x'];
                let y = object['grid_new_y'];
                let w = object['w'];
                let h = object['h'];
                for (let j = 0; j < h; j++) {
                    if (obj[y + j] == undefined) {
                        obj[y + j] = [];
                    }
                    for (let i = 0; i < w; i++) {
                        obj[y + j][x + i] = 1;
                    }
                }
            }
        });
        return obj;
    },
    findEmptyAround: function (map_arr, obj) {
        var empty = [];
        for (let j in obj) {
            j = parseInt(j);
            for (let i in obj[j]) {
                i = parseInt(i);
                if (obj[j][i] == 1) {
                    let jvars = [j - 1, j, j + 1];
                    let ivars = [i - 1, i, i + 1];
                    for (let jvar in jvars) {
                        let row = jvars[jvar];
                        for (let ivar in ivars) {
                            let col = ivars[ivar];
                            let map_has_coord = map_arr[row] != undefined && map_arr[row][col] != undefined;
                            let empty_cell = map_has_coord && map_arr[row][col] != 1;
                            if (empty_cell) {
                                if (empty[row] == undefined) {
                                    empty[row] = [];
                                }
                                empty[row][col] = 1;
                            }
                        }
                    }
                }
            }
        }
        return empty;
    },
    chooseTargetPoint: function (empty_points) {
        for (let j in empty_points) {
            j = parseInt(j);
            for (let i in empty_points[j]) {
                i = parseInt(i);
                if (empty_points[j][i] == 1) {
                    return [j, i];
                }
            }
        }
    },
    addTargetPoints: function (map_arr, start_point, end_point) {
        map_arr[start_point[0]][start_point[1]] = 2;
        map_arr[end_point[0]][end_point[1]] = 3;
        return map_arr;
    },
    findPath: function (map_arr) {
        let values = { 0: 'w', 1: 'u', 2: 's', 3: 'g' };
        let walkable = [];
        _.each(map_arr, function (value, rindex) {
            _.each(value, function (ivalue, cindex) {
                if (walkable[rindex] == undefined) {
                    walkable[rindex] = [];
                }
                walkable[rindex][cindex] = values[ivalue];
            });
        });
        if(debug) {
         ///   console.log("walkable", walkable);
        }
        let path = astar(walkable, path_algo_type, path_cut_corner);
        return path;
    },
    convertPath: function (map_spec, path) {
        let coords = [];
        _.each(path, function (value, index) {
            coords[index] = {};
            coords[index].cell_x = value.row; //(value.row * map_spec['cell_size']);
            coords[index].cell_y = value.col; //(value.col * map_spec['cell_size']);
        });
        return coords;
    },
    drawPath: function (map_arr, path) {
        _.each(path, function (value) {
            let not_start = map_arr[value.row][value.col] != 2;
            let not_end = map_arr[value.row][value.col] != 3;
            if (not_start && not_end) {
                map_arr[value.row][value.col] = 4;
            }
        });
        return map_arr;
    }
};
