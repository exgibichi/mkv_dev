'use strict';

var map_lat = 0.015625; // 32/2048
var map_lng = 0.015625;
var fs = require('fs');
const path = require('path');
var Promise = require("bluebird");
var debug = 0;
var precision = 8;

module.exports = {
    getMapSpec: function(map) {
        let map_spec = {};
        map_spec['zero_lat'] = map['zero_point_y'];
        map_spec['scale'] = map['scale'];
        map_spec['lat'] = map_lat;
        map_spec['lng'] = map_lng;
        map_spec['zero_lng'] = map['zero_point_x'];
        return map_spec;
    },
    compile: function (map, nodes, map_dir, points_file, cb) {
        let map_spec = this.getMapSpec(map);
        let tpl = `function addPoints() {
            var tpl = \`
            <table data-id="{id}">
            <tr cols="2">
            <td>
            {title}
            </td>
            </tr>
            <tr cols="2">
            <td>
            {desc}
            </td>
            </tr>
            <tr>
            <td>
            <a href="" class="from">Отсюда</a>
            </td>
            <td>
            <a href="" class="to">Сюда</a>
            </td>
            </tr>
            </table>
            \`;
            {objects_polygons}
        }`;
        if (debug) {
            console.log('map', map);
            console.log('nodes', nodes);
            console.log('map_dir', map_dir);
            console.log('points_file', points_file);
            console.log('precision', precision);
            console.log('map_spec', map_spec);
        }
        let self = this;
        return this.getPolygonsData(map_spec, nodes).then(function (nodes_data) {
            if (debug) {
                console.log('nodes_data', nodes_data);
            }
            return self.getPolygons(nodes_data).then(function (objects_polygons) {
                if (debug) {
                    console.log('objects_polygons', objects_polygons);
                }
                fs.writeFile(
                    map_dir + points_file,
                    tpl.replace('{objects_polygons}', objects_polygons.join("\r\n")),
                    (err) => {
                        if (err) {
                            console.log('The objects file save err', err);
                        }
                        console.log('The objects file has been saved!');
                        cb(null, true);
                    });
                });
            }
        );
    },
    getPolygonsData: function(map_spec, nodes) {
        if(debug) {
            console.log("getPolygonsData start");
        }
        let ploygons_data = [];
        let self = this;
        return Promise.map(nodes, function (node, node_index) {
            return new Promise(function (resolve, reject) {
                if (node['size'] == undefined) { resolve(true); return; }
                let node_pos = node['pos'].split(' ');
                let node_size = node['size'].split(' ');
                self.computePoint(map_spec, node_pos, node_size).then(function(point) {
                    ploygons_data.push({
                        'id': node['id'],
                        'point': point,
                        'name': node['name'] != undefined ? node['name'] : '',
                        'desc': node['desc'] != undefined ? node['desc'] : ''
                    });
                    resolve(true);
                });
            });
        }).then(function() {
            return ploygons_data;
        });
    },
    getPolygons: function(polygons_data) {
        let polygons = [];
        return Promise.map(polygons_data, function (polygon, polygon_index) {
            let title = polygon['name'];
            let desc = polygon['desc'];
            let id = polygon['id'];
            polygons.push(`var polygon` + id + ` = L
            .polygon(` + JSON.stringify(polygon.point['map_render']) + `)
            .bindPopup(tpl.replace('{title}', '` + title + `')
            .replace('{desc}', '` + desc + `')
            .replace('{id}', '` + id + `'))
            .on('mouseover', function (e) { this.openPopup(); })
            .addTo(map);`);
        }).then(function(){
            return polygons;
        });
    },
    computeCoord: function (map_spec, coord) {
        let orig_lat = coord[0];
        let orig_lng = coord[1];
        let zero_lat = map_spec['zero_lat'] * (-1);
        let zero_lng = map_spec['zero_lng'];
        let lat = (orig_lat - zero_lat) * map_spec['lat'];
        let lng = (orig_lng - zero_lng) * map_spec['lng'];
        if (debug) {
            console.log('precision', precision);
            console.log('orig_lat', orig_lat);
            console.log('orig_lng', orig_lng);
            console.log('zero_lat', zero_lat);
            console.log('zero_lng', zero_lat);
            console.log('map_lat', map_lat);
            console.log('map_lng', map_lng);
            console.log('lat', lat);
            console.log('lng', lng);
        }
        lat += map_lat * 4;
        lng -= map_lng * 4;
        lat = lat.toPrecision(precision);
        lng = lng.toPrecision(precision);
        return [lat, lng];
    },
    computePoint: function (map, node_pos, node_size) {
        let node_lat = parseFloat(node_pos[1] * (-1));
        let node_lng = parseFloat(node_pos[0]);
        let node_width = parseFloat(node_size[1]);
        let node_height = parseFloat(node_size[0]);
        let coords = {};
        coords['orig'] = [];
        coords['orig'].push([node_lat, node_lng]);
        coords['orig'].push([node_lat, node_lng + node_height]);
        coords['orig'].push([node_lat - node_width, node_lng + node_height]);
        coords['orig'].push([node_lat - node_width, node_lng]);
        if(debug) {
            console.log('orig', coords);
        }
        coords['map_render'] = [];
        let self = this;
        return Promise.map(coords['orig'], function(coord) {
            return new Promise(function (resolve, reject) {
                let new_coord = self.computeCoord(map, coord);
                coords['map_render'].push(new_coord);
                resolve(true);
            });
        }).then(function() {
            return coords;
        });
    }
}