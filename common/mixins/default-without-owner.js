'use strict';

module.exports = function(Model, options) {
  Model.defineProperty('created_at', {type: Date, default: '$now'});
  Model.defineProperty('updated_at', {type: Date, default: '$now'});
};
