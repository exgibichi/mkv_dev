'use strict';

module.exports = function(Model, options) {
  Model.defineProperty('owner_id', {type: "number", required: true, default: 1});
  Model.defineProperty('created_at', {type: Date, default: '$now'});
  Model.defineProperty('updated_at', {type: Date, default: '$now'});
};
