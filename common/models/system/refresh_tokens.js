'use strict';

var uid = require('uid2');
var DEFAULT_TOKEN_LEN = 64;

module.exports = function(Refresh_tokens) {

    Refresh_tokens.createRefreshTokenId = function (fn) {
        uid(DEFAULT_TOKEN_LEN, function (err, guid) {
            if (err) {
                fn(err);
            } else {
                fn(null, guid);
            }
        });
    };

};
