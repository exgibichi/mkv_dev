'use strict';

var multer = require('multer');
var fs = require('fs');
var _ = require('lodash');
const path = require('path');
const cm = require('../../libs/compile_map');
const cp = require('../../libs/compile_points');
const fp = require('../../libs/find_path');
var debug = 1;

module.exports = function(maps) {
    
    maps.compile = function(map_id, res) {
        let map = this.app.models.maps;
        let objects = this.app.models.objects;
        map.findOne({"where": {"id": map_id}}, function(err, model) {
           /*
            cm.compile(model, function(err, result) {
                console.log('cm result: ', err, result);
                if(err) {
                    res.json({"status": false, "data": err});
                    return;
                }
             //*/
                objects.find({ "where": { "map_id": map_id } }, function (err, points) {
                    if (err) {
                        res.json({ "status": false, "data": err });
                        return;
                    }
                    let points_file = 'points.js';
                    let map_dir = path.join(__dirname, '..', '..', '..', 'client', 'maps', map_id) + '/';
                    cp.compile(model, points, map_dir, points_file, function(err, result) {
                        if (err) {
                            res.json({ "status": false, "data": err });
                            return;
                        }
                        console.log('cp result: ', err, result);
                        res.json({ "status": true, "data": true });
                    });
                    /*
               });
               //*/
            });
        });
    };
    
    maps.findPath = function(map_id, from_id, to_id, res) {
        let map = this.app.models.maps;
        let objects = this.app.models.objects;
        let points = [];
        map.findOne({ "where": { "id": map_id}}, function(err, model) {
            objects.find({ "where": { "map_id": map_id } }, function (err, map_objects) {
                fp.find(model, map_objects, from_id, to_id, function(err, way) {
                    let map_spec = cp.getMapSpec(model);
                    _.each(way, function(point) {
                        let map_point = cp.computeCoord(
                            map_spec,
                            [
                                (point['cell_x']) * (-1),
                                (point['cell_y'])
                            ]
                        );
                        let koef = 0.015625 * 50;
                        let x = (parseFloat(point['cell_x']) * (-1)) * koef;
                        let y = parseFloat(point['cell_y']) * koef;
                        points.push([x, y]);
                    });
                    if (debug) {
                        console.log('map_spec: ', map_spec);
                        console.log('fp result: ', err, way);
                        console.log('points: ', points);
                    }
                    res.json({ "status": true, "data": points});
                });
            });
        });
    };
    
    let uploaded_file_name = '';
    let original_name = '';
    let dirPath;
    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            dirPath = path.join(__dirname, '..', '..', '..', 'client', 'maps', req.body.map_id) + '/';
            console.log('dp', dirPath);
            let dir_exist = fs.existsSync(dirPath);
            if (!dir_exist) {
                let dir = fs.mkdirSync(dirPath);
            }
            cb(null, dirPath);
        },
        filename: function (req, file, cb) {
            var fileName = 'm' + req.body.map_id + '.png';
            cb(null, fileName);
        }
    });
    maps.upload = function (req, res, cb) {
        var self = this;
        var upload = multer({
            storage: storage
        }).single('file');
        upload(req, res, function (err) {
            if (err) {
                return res.json({ "upload": err.message });
            }
            return res.json({ "status": true, "data": true });
        });
    };
    maps.remoteMethod('compile', {
        accepts: [
            {
                arg: 'map_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:map_id/compile', verb: 'get' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    maps.remoteMethod('findPath', {
        accepts: [
            {
                arg: 'map_id',
                type: 'string',
                required: true
            },
            {
                arg: 'from',
                type: 'string',
                required: true
            },
            {
                arg: 'to',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/findPath', verb: 'get' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    maps.remoteMethod('upload', {
        accepts: [{
            arg: 'req',
            type: 'object',
            http: {
                source: 'req'
            }
        }, {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }],
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
};
