'use strict';

module.exports = function(Exponats) {
    Exponats.stat = function(req, res, cb) {
        var ds = Exponats.dataSource;
        var sql = `
        SELECT t.title, count(et.exponat_id) as count
        FROM exponats_types et JOIN types t
        ON et.type_id = t.id
        GROUP BY et.type_id`;
        ds.connector.execute(sql, function(err, types) {
            if (err) throw err;
            return res.json({"status": true, "data": types});
        });
    };
    Exponats.addFeature = function(exponat_id, rel_id, res) { // TODO abstract all rel ops
        let rel_contacts = this.app.models.exponats_features;
        rel_contacts.create({ feature_id: rel_id, exponat_id: exponat_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };

    Exponats.deleteFeature = function(exponat_id, rel_id, res) {
        let rel_contacts = this.app.models.exponats_features;
        rel_contacts.destroyAll({ feature_id: rel_id, exponat_id: exponat_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };

    Exponats.addType = function(exponat_id, rel_id, res) {
        let rel_addresses = this.app.models.exponats_types;
        rel_addresses.create({ type_id: rel_id, exponat_id: exponat_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };

    Exponats.deleteType = function(exponat_id, rel_id, res) {
        let rel_addresses = this.app.models.exponats_types;
        rel_addresses.destroyAll({ type_id: rel_id, exponat_id: exponat_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };

    Exponats.remoteMethod('addFeature', {
        accepts: [
            {
                arg: 'exponat_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:exponat_id/features/:rel_id', verb: 'post' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );

    Exponats.remoteMethod('deleteFeature', {
        accepts: [
            {
                arg: 'exponat_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:exponat_id/features/remove/:rel_id', verb: 'delete' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
    Exponats.remoteMethod('addType', {
        accepts: [
            {
                arg: 'exponat_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:exponat_id/types/:rel_id', verb: 'post' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );

    Exponats.remoteMethod('deleteType', {
        accepts: [
            {
                arg: 'exponat_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:exponat_id/types/remove/:rel_id', verb: 'delete' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
    Exponats.remoteMethod('stat', {
        accepts: [
        {
            arg: 'req',
            type: 'object',
            http: {
                source: 'req'
            }
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {verb: "get"},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
};
