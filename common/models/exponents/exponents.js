'use strict';
var Promise = require("bluebird");
var fs = require('fs');
const path = require('path');
var YAML = require('json2yaml');

//let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static', 'posts') + '/';
let dirPath = path.join(__dirname, '..', '..', '..', '..', 'mkv_site', 'source', '_posts') + '/';

module.exports = function(Exponents) {
    
    let fields = {
        "types": "type",
        "categories": "category",
        "companies": "company",
        "objects": "object"
    };
    
    var modelEquals = function(x) {
        for (var p in this) {
            if (typeof (this[p]) !== typeof (x[p])) return false;
            if ((this[p] === null) !== (x[p] === null)) return false;
            switch (typeof (this[p])) {
                case 'undefined':
                if (typeof (x[p]) != 'undefined') return false;
                break;
                case 'object':
                if (this[p] !== null && x[p] !== null && (this[p].constructor.toString() !== x[p].constructor.toString() || !this[p].equals(x[p]))) return false;
                break;
                case 'function':
                if (p != 'equals' && this[p].toString() != x[p].toString()) return false;
                break;
                default:
                if (this[p] !== x[p]) return false;
            }
        }
        return true;
    };
    
    var getData = function(model, value) {
        if (value === "" || value === undefined || value === null) {
            return new Promise.resolve();
        } else {
            if (Array.isArray(value)) {
                return Promise.map(Object.keys(value), function (rel_entry_key) {
                    var rel_entry = value[rel_entry_key];
                    rel_entry.owner_id = 1;
                    return new Promise(function (resolve, reject) {
                        if (rel_entry.id !== undefined) {
                            model.findById(rel_entry.id, function (err, res) {
                                if (err) { reject(err); }
                                res.updateAttributes(rel_entry, function (err, res) {
                                    if (err) { reject(err); }
                                    resolve(res);
                                });
                            });
                        } else {
                            model.findOrCreate(rel_entry, function (err, res) {
                                if (err) { reject(err); }
                                resolve(res);
                            });
                        }
                    });
                }).then(function (res) {
                    return res;
                });
            } else if (typeof value === "object") {
                value.owner_id = 1;
                return new Promise(function (resolve, reject) {
                    if (value.id !== undefined) {
                        model.findById(value.id, function(err, res) {
                            if (err) { reject(err); }
              
                                res.updateAttributes(value, function (err, res) {
                                    if (err) { reject(err); }
                                    resolve(res);
                                });
                            
                        });
                    } else {
                        model.findOrCreate(value, function (err, res) {
                            if (err) { reject(err); }
                            resolve(res);
                        });
                    }
                });
            } else {
                return new Promise.resolve();
            }
        }
    };
    
    var getValue = function(raw, field = 'id') {
        if (raw == null) {
            return null;
        }
        if (Array.isArray(raw)) {
            return raw[0][field];
        } else {
            return raw[field];
        }
    };
    
    var preSave = function(models, entry) {
        return Promise.map(Object.keys(fields), function(field) {
            let key = fields[field];
            let relation = null;
            if (entry[key] != undefined && typeof entry[key] != "function") {
                relation = entry[key];
            } else if (entry.__cachedRelations != undefined && entry.__cachedRelations[key] != undefined) {
                relation = entry.__cachedRelations[key];
            } else {
                return;
            }
            return getData(models[field], relation, 'id').then(
                function(value) {
                    entry[key + "_id"] = getValue(value, 'id');
                }
            );
        }).then(function(res) {
            return entry;
        });
    };
    
    Exponents.observe('before save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        preSave(models, data).then(function(v) {
            next();
        });
    });
    
    Exponents.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        data.reload(function(err, mdl){
            if(ctx.instance !== undefined) {
                ctx.instance = mdl;
            } else {
                ctx.data = mdl;
            }
            let dir_exist = fs.existsSync(dirPath);
            if (!dir_exist) {
                let dir = fs.mkdirSync(dirPath);
            }
            Exponents.findById(mdl.id, function (err, model) {
                let ymlText;
                let data_obj = JSON.parse(JSON.stringify(model));
                data_obj.title = data_obj.company.name;
                data_obj.layout = "company-page";
                data_obj.theme = data_obj.category.title;
                data_obj.category = "members";
                ymlText = YAML.stringify(data_obj) + "\r\n---";
                fs.writeFile(dirPath + 'company_' + mdl.id + '.md', ymlText, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            });
        });
        next();
    });

    Exponents.observe('after delete', function (ctx, next) {
        if (ctx.where.id != undefined) {
            fs.unlink(dirPath + 'company_' + ctx.where.id + '.md', function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        }
        next();
    });
};
