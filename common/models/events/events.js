'use strict';

var fs = require('fs');
const path = require('path');
var YAML = require('json2yaml');
//let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static', 'posts') + '/';
let dirPath = path.join(__dirname, '..', '..', '..', '..', 'mkv_site', 'source', '_posts') + '/';

module.exports = function(Events) {
    Events.resaveAll = function (passwd, res) {
        
        if (passwd != 'rgsh') {
            return res.json({ "status": false, "data": false });
        }
        var resave = function (err, models) {
            if (models == null) {
                console.log("models null");
            } else {
                models.map(function (model) {
                    model.save();
                });
            }
        };
        let events = this.app.models.events;
        let exponents = this.app.models.exponents;
        let partners = this.app.models.partners;
        let offnews = this.app.models.offnews;
        let themes = this.app.models.themes;
        offnews.find({}, resave);
        events.find({}, resave);
        exponents.find({}, resave);
        partners.find({}, resave);
        themes.find({}, resave);
        return res.json({ "status": true, "data": true });
        
    };
    Events.addProfile = function(event_id, rel_id, res) {
        let rel_contacts = this.app.models.events_profiles;
        rel_contacts.create({ profile_id: rel_id, event_id: event_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Events.deleteProfile = function(event_id, rel_id, res) {
        let rel_contacts = this.app.models.events_profiles;
        rel_contacts.destroyAll({ profile_id: rel_id, event_id: event_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Events.addCompany = function(event_id, rel_id, res) {
        let rel_addresses = this.app.models.events_companies;
        rel_addresses.create({ company_id: rel_id, event_id: event_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Events.deleteCompany = function(event_id, rel_id, res) {
        let rel_addresses = this.app.models.events_companies;
        rel_addresses.destroyAll({ company_id: rel_id, event_id: event_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Events.dates = function(event_type, res, cb) {
        var ds = Events.dataSource;
        event_type = event_type == "1" ? 1 : 2;
        var sql = 'SELECT DISTINCT DATE(start_time) AS date from events where event_type = ' + event_type;
        ds.connector.execute(sql, function(err, dates) {
            if (err) throw err;
            return res.json({ "status": true, "data": dates });
        });
    };
    
    Events.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        let dir_exist = fs.existsSync(dirPath);
        if (!dir_exist) {
            let dir = fs.mkdirSync(dirPath);
        }
        Events.findById(data.id, function(err, model) {
            let ymlText;
            if (model.event_type == 2) {
                model.category = "demo";
                let data_obj = JSON.parse(JSON.stringify(model));
                ymlText = YAML.stringify(data_obj) + "\r\n---";
                fs.writeFile(dirPath + 'demo_' + model.id + '.md', ymlText, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            } else {
                model.category = "ndp";
                let data_obj = JSON.parse(JSON.stringify(model));
                ymlText = YAML.stringify(data_obj) + "\r\n---";
                fs.writeFile(dirPath + 'ndp_' + model.id + '.md', ymlText, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            }
        });
        next();
    });
    
    Events.observe('after delete', function (ctx, next) {
        if (ctx.where.id != undefined) {
            let demo_path = dirPath + 'demo_' + ctx.where.id + '.md';
            let ndp_path = dirPath + 'ndp_' + ctx.where.id + '.md';
            let demo_exist = fs.existsSync(demo_path);
            let ndp_exist = fs.existsSync(ndp_path);
            if (demo_exist) {
                fs.unlink(demo_path, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            }
            if (ndp_exist) {
                fs.unlink(ndp_path, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            }
        }
        next();
    });
    
    Events.remoteMethod('resaveAll', {
        accepts: [
            {
                arg: 'passwd',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { verb: "get" },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Events.remoteMethod('dates', {
        accepts: [
            {
                arg: 'event_type',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { verb: "get" },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Events.remoteMethod('addProfile', {
        accepts: [
            {
                arg: 'event_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:event_id/profiles/:rel_id', verb: 'post' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
);

Events.remoteMethod('deleteProfile', {
    accepts: [
        {
            arg: 'event_id',
            type: 'string',
            required: true
        },
        {
            arg: 'rel_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:event_id/profiles/remove/:rel_id', verb: 'delete' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);
Events.remoteMethod('addCompany', {
    accepts: [
        {
            arg: 'event_id',
            type: 'string',
            required: true
        },
        {
            arg: 'rel_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:event_id/companies/:rel_id', verb: 'post' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);

Events.remoteMethod('deleteCompany', {
    accepts: [
        {
            arg: 'event_id',
            type: 'string',
            required: true
        },
        {
            arg: 'rel_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:event_id/companies/remove/:rel_id', verb: 'delete' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);
};
