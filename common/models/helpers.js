'use strict';
var _ = require('lodash');

module.exports = function(Helpers) {

    Helpers.types = function(req, res, cb) {
        var ds = Helpers.dataSource;
        var sql = "SELECT DISTINCT type FROM " + Helpers.modelName;
        ds.connector.execute(sql, function(err, types) {
            if (err) throw err;
            return res.json({"status": true, "data": _.map(types, 'type')});
        });
    };

    Helpers.getListByType = function(type, res, cb) {
        Helpers.find({where: {type: type}}, function(err, entries) {
            if (err) throw err;
            return res.json({"status": true, "data": entries});
        });
    };

    Helpers.remoteMethod('types', {
        accepts: [{
            arg: 'req',
            type: 'object',
            http: {
                source: 'req'
            }
        }, {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }],
        http: {verb: "get"},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });

    Helpers.remoteMethod('getListByType', {
        accepts: [
        {
            arg: 'type',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:type', verb: 'get'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
};
