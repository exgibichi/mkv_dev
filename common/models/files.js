'use strict';

var multer = require('multer');
var fs = require('fs');
const path = require('path');

module.exports = function(files) {
    let uploaded_file_name = '';
    let original_name = '';
    let types = ['images', 'docs', 'videos'];
    let dirPath;
    let storage = multer.diskStorage({
        destination: function(req, file, cb) {
            if (req.body.type === undefined || types.indexOf(req.body.type) === -1) {
                console.log("no type"); // TODO error proccessing
                cb({"err": "no type"});
                return;
            }
            dirPath = path.join(__dirname, '..', '..', 'client', 'files') + '/';
            console.log('dp', dirPath);
            let dir_exist = fs.existsSync(dirPath);
            if (!dir_exist) {
                let dir = fs.mkdirSync(dirPath);
            }
            dirPath += req.body.type;
            let t = +Date.now() + "";
            let subdir = t.slice(-2);
            let subsubdir = t.slice(-4, -2);
            dir_exist = fs.existsSync(dirPath);
            if (!dir_exist) {
                let dir = fs.mkdirSync(dirPath);
            }
            dirPath += "/" + subdir;
            dir_exist = fs.existsSync(dirPath);
            if (!dir_exist) {
                let dir = fs.mkdirSync(dirPath);
            }
            dirPath += "/" + subsubdir;
            dir_exist = fs.existsSync(dirPath);
            if (!dir_exist) {
                let dir = fs.mkdirSync(dirPath);
            }
            dirPath += '/';
            cb(null, dirPath);
        },
        filename: function(req, file, cb) {
            original_name = file.originalname;
            var ext = file.originalname.substring(file.originalname.lastIndexOf("."));
            var fileName = Date.now() + ext;
            uploaded_file_name = fileName;
            cb(null, fileName);
        }
    });
    files.upload = function(req, res, cb) {
        var self = this;
        var upload = multer({
            storage: storage
        }).single('file');
        upload(req, res, function(err) {
            if (err) {
                return res.json({"upload": err.message});
            }
            if (req.accessToken == null || req.accessToken.userId === undefined) {
                return res.json({ "upload": "not logged in" });
            }
            self.app.models.files.create([{
                "url": dirPath.replace(path.join(__dirname, '..', '..', 'client'), '') + uploaded_file_name,
                "file": original_name,
                "type": req.body.type,
                "title": req.body.title,
                "owner_id": req.accessToken.userId,
            }], function(err, files) {
                if (err) {
                    throw err;
                }
                return res.json({"status": true, "data": files[0]});
            });
        });
    };
    files.remoteMethod('upload', {
        accepts: [{
            arg: 'req',
            type: 'object',
            http: {
                source: 'req'
            }
        }, {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }],
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
};
