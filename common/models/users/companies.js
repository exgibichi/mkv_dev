'use strict';

var Promise = require("bluebird");
var _ = require("lodash");
var fs = require('fs');
const path = require('path');
var YAML = require('json2yaml');

module.exports = function(Companies) {
    let fields = {
        "addresses": "addresses",
        "contacts": "contacts"
    };
    let mtm_rels = {
        "addresses": "companies_addresses",
        "contacts": "companies_contacts"
    };
    let company_id = [];
    var ad_ids = {};
    var ct_ids = {};
    
    var getValue = function(raw, field = 'id') {
        if (raw == null || raw == undefined) {
            return null;
        }
        if (Array.isArray(raw)) {
            return raw[0][field];
        } else {
            return raw[field];
        }
    };
    
    var preOut = function(helpers, entry) {
        return new Promise(function (resolve, reject) {
            entry.country = null;
            resolve(entry);
        });
    };
    
    var modelEquals = function (x) {
        for (var p in this) {
            if (typeof (this[p]) !== typeof (x[p])) return false;
            if ((this[p] === null) !== (x[p] === null)) return false;
            switch (typeof (this[p])) {
                case 'undefined':
                if (typeof (x[p]) != 'undefined') return false;
                break;
                case 'object':
                if (this[p] !== null && x[p] !== null && (this[p].constructor.toString() !== x[p].constructor.toString() || !this[p].equals(x[p]))) return false;
                break;
                case 'function':
                if (p != 'equals' && this[p].toString() != x[p].toString()) return false;
                break;
                default:
                if (this[p] !== x[p]) return false;
            }
        }
        return true;
    };
    
    var getData = function (model, value) {
        if (value === "" || value === undefined || value === null) {
            return new Promise.resolve();
        } else {
            if (Array.isArray(value)) {
                return Promise.map(Object.keys(value), function (rel_entry_key) {
                    var rel_entry = value[rel_entry_key];
                    rel_entry.owner_id = 1;
                    return new Promise(function (resolve, reject) {
                        if (rel_entry.id !== undefined) {
                            model.findById(rel_entry.id, function (err, res) {
                                if (err) { reject(err); }
                                res.updateAttributes(rel_entry, function (err, res) {
                                    if (err) { reject(err); }
                                    resolve(res);
                                });
                            });
                        } else {
                            model.findOrCreate(rel_entry, function (err, res) {
                                if (err) { reject(err); }
                                resolve(res);
                            });
                        }
                    });
                }).then(function (res) {
                    return res;
                });
            } else if (typeof value === "object") {
                value.owner_id = 1;
                return new Promise(function (resolve, reject) {
                    if (value.id !== undefined) {
                        model.findById(value.id, function (err, res) {
                            if (err) { reject(err); }
                            res.updateAttributes(value, function (err, res) {
                                if (err) { reject(err); }
                                resolve(res);
                            });
                        });
                    } else {
                        model.findOrCreate(value, function (err, res) {
                            if (err) { reject(err); }
                            resolve(res);
                        });
                    }
                });
            } else {
                return new Promise.resolve();
            }
        }
    };
    
    var preSave = function (models, entry) {
        return Promise.map(Object.keys(fields), function (field) {
            let key = fields[field];
            let relation;
            if (entry[key] != undefined && typeof entry[key] != "function") {
                relation = entry[key];
            } else if (entry.__cachedRelations != undefined && entry.__cachedRelations[key] != undefined) {
                relation = entry.__cachedRelations[key];
            } else {
                return;
            }
            return getData(models[field], relation, 'id').then(
                function (value) {
                    var vx = value;
                    if (field == "addresses" && value != undefined) {
                        if (ad_ids[entry.id] === undefined) {
                            ad_ids[entry.id] = [];
                        }
                        if(Array.isArray(value)) {
                            _.map(value, function(v){
                                ad_ids[entry.id].push(getValue(v, 'id'));
                            });
                        } else {
                            ad_ids[entry.id].push(getValue(value, 'id'));
                        }
                    }
                    if (field == "contacts" && value != undefined) {
                        if (ct_ids[entry.id] === undefined) {
                            ct_ids[entry.id] = [];
                        }
                        if (Array.isArray(value)) {
                            _.map(value, function (v) {
                                ct_ids[entry.id].push(getValue(v, 'id'));
                            });
                        } else {
                            ct_ids[entry.id].push(getValue(value, 'id'));
                        }
                    }
                }
            );
        }).then(function (res) {
            return entry;
        });
    };
    
    Companies.observe('loaded', function logQuery(ctx, next) {
        let helpers = ctx.Model.app.models.helpers;
        preOut(helpers, ctx.data).then(function(v) {
            next();
        });
    });
    Companies.observe('before save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        ad_ids = {};
        ct_ids = {};
        preSave(models, data).then(function(v) {
            next();
        });
    });

    Companies.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        company_id = ctx.instance.id;
        if (ad_ids[company_id] !== undefined && ad_ids[company_id].length > 0) {
            _.map(ad_ids[company_id], function(ad_id) {
                models.companies_addresses.findOrCreate({company_id: company_id, address_id: ad_id});
            });
        }
        if (ct_ids[company_id] !== undefined && ct_ids[company_id].length > 0) {
            _.map(ct_ids[company_id], function(ct_id) {
                models.companies_contacts.findOrCreate({company_id: company_id, contact_id: ct_id});
            });
        }
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        /*
        //let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static', 'posts') + '/';
        let dirPath = path.join(__dirname, '..', '..', '..', '..', 'mkv_site', 'source', '_posts') + '/';
        let dir_exist = fs.existsSync(dirPath);
        if (!dir_exist) {
            let dir = fs.mkdirSync(dirPath);
        }
        Companies.findById(data.id, function (err, model) {
            model.title = data.name;
            model.category = "members";
            model.layout = "company-page";
            let ymlText;
            let data_obj = JSON.parse(JSON.stringify(model));
            ymlText = YAML.stringify(data_obj) + "\r\n---";
            fs.writeFile(dirPath + 'company_' + data.id + '.md', ymlText, function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        });
        */
        next();
    });
    Companies.addContact = function(company_id, rel_id, res) {
        console.log('gi', company_id, 'ii', rel_id);
        let rel_contacts = this.app.models.companies_contacts;
        rel_contacts.create({ contact_id: rel_id, company_id: company_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    Companies.deleteContact = function(company_id, rel_id, res) {
        console.log('gi', company_id, 'ii', rel_id);
        let rel_contacts = this.app.models.companies_contacts;
        rel_contacts.delete({ contact_id: rel_id, company_id: company_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    Companies.addAddress = function(company_id, rel_id, res) {
        console.log('gi', company_id, 'ii', rel_id);
        let rel_addresses = this.app.models.companies_addresses;
        rel_addresses.create({ address_id: rel_id, company_id: company_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Companies.deleteAddress = function(company_id, rel_id, res) {
        console.log('gi', company_id, 'ii', rel_id);
        let rel_addresses = this.app.models.companies_addresses;
        rel_addresses.delete({ address_id: rel_id, company_id: company_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Companies.remoteMethod('addContact', {
        accepts: [
            {
                arg: 'company_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:company_id/contacts/:rel_id', verb: 'post' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
);

Companies.remoteMethod('deleteContacts', {
    accepts: [
        {
            arg: 'company_id',
            type: 'string',
            required: true
        },
        {
            arg: 'rel_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:company_id/contacts/remove/:rel_id', verb: 'delete' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);
Companies.remoteMethod('addAddress', {
    accepts: [
        {
            arg: 'company_id',
            type: 'string',
            required: true
        },
        {
            arg: 'rel_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:company_id/addresses/:rel_id', verb: 'post' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);

Companies.remoteMethod('deleteAddress', {
    accepts: [
        {
            arg: 'company_id',
            type: 'string',
            required: true
        },
        {
            arg: 'rel_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:company_id/addresses/remove/:rel_id', verb: 'delete' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);
};
