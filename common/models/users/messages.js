'use strict';
var admin = require("firebase-admin");

module.exports = function(Messages) {
    
    var preOut = function(pc, chat_id, sender_id) {
        return pc.find({"where":{"and": [{"chat_id": chat_id}, {"profile_id": {"neq": sender_id}}]}}).then(function(res){
            return res[0].profile_id;
        });
    };
    
    Messages.observe('loaded', function logQuery(ctx, next) {
        let pc = ctx.Model.app.models.profiles_chats;
        preOut(pc, ctx.data.chat_id, ctx.data.owner_id).then(function(pid) {
            ctx.data.profile_id = pid;
            next();
        });
    });
    
    Messages.observe('before save', function logQuery(ctx, next) {
        let chats = ctx.Model.app.models.chats;
        let profiles_chats = ctx.Model.app.models.profiles_chats;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        if (data.profile_id != undefined) {
            var ds = Messages.dataSource;
            var sql = 'select chat_id from profiles_chats where profile_id = ' + data.profile_id + ' and chat_id in (select chat_id from profiles_chats where profile_id = ' + data.owner_id + ')';
            ds.connector.execute(sql, function(err, chat) {
                if (!chat.length) {
                    chats.create({}, function(err, chat) {
                        profiles_chats.findOrCreate({"profile_id": data.profile_id, "chat_id": chat.id}, function(err, mdl) {
                            profiles_chats.findOrCreate({"profile_id": data.owner_id, "chat_id": chat.id}, function(err, mdl) {
                                data.chat_id = chat.id;
                                next();
                            });
                        });
                    });
                } else {
                    data.chat_id = chat[0].chat_id;
                    next();
                }
            });
        } else {
            next();
        }
    });
    
    Messages.observe('after save', function logQuery(ctx, next) {
        Messages.app.models.profiles.findById(ctx.instance.profile_id).then(function(profile) {
            if (profile.push_token != null) {
                var payload = {
                    notification: {
                        title: "новое сообщение",
                        body: ctx.instance.message
                    },
                    data: {
                        type: "message",
                        data: JSON.stringify(ctx.instance.__data)
                    }
                };
                admin.messaging().sendToDevice(profile.push_token, payload)
                .then(function(response) {
                    console.log("Successfully sent message:", response.results);
                })
                .catch(function(error) {
                    console.log("Error sending message:", error);
                });
            }
        });
        next();
    });
    
    Messages.getMessagesForProfile = function(profile_id, res, req) {
        if (req.accessToken != null && profile_id != req.accessToken.userId) {
            let pc = this.app.models.profiles_chats;
            let second_user_id = req.accessToken.userId;
            var ds = Messages.dataSource;
            var sql = 'select chat_id from profiles_chats where profile_id = ' + profile_id + ' and chat_id in (select chat_id from profiles_chats where profile_id = ' + second_user_id + ')';
            ds.connector.execute(sql, function(err, types) {
                if (err) throw err;
                if (types.length) {
                    Messages.find({ "where": { "chat_id": types[0].chat_id}}).then(function(ms){
                        return res.json({ "status": true, "data": ms });
                    });
                } else {
                    return res.json({ "status": true, "data": [] });
                }
            });
        } else {
            return res.json({ "status": false, "data": "no token" });
        }
    };
    
    Messages.remoteMethod('getMessagesForProfile', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            },
            {
                arg: 'req',
                type: 'object',
                http: {
                    source: 'req'
                }
            }
        ],
        http: { path: '/:profile_id/profile', verb: 'get' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
};
