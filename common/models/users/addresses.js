'use strict';

var Promise = require("bluebird");

module.exports = function(Addresses) {
    let fields = {
        "streets": "street",
        "cities": "city",
        "countries": "country"
    };
    
    var getHelperData = function(helper, type, value, byField = 'id') {
        if (value === "" || value === undefined || value === null) {
            return new Promise.resolve();
        } else {
            if (byField == 'id') {
                let data = {name: value, type: type};
                let create_data = {name: value, type: type, owner_id: 1};
                return helper.findOrCreate({where: data}, create_data);
            } else {
                return helper.findById(value);
            }
        }
    };
    
    var getValue = function(raw, field = 'id') {
        if (raw == null) {
            return null;
        }
        if (Array.isArray(raw)) {
            return raw[0][field];
        } else {
            return raw[field];
        }
    };
    
    var preSave = function(helpers, entry) {
        return Promise.map(Object.keys(fields), function(field) {
            let key = fields[field];
            return getHelperData(helpers, field, entry[key], 'id').then(
            function(value) {
                entry[key + "_id"] = getValue(value, 'id');
            });
        }).then(function(res) {
            return entry;
        });
    };
    
    var preOut = function(helpers, entry) {     
        return Promise.map(Object.keys(fields), function(field) {
            let key = fields[field];
            return getHelperData(helpers, field, entry[key + "_id"], 'name').then(
            function(value) {
                entry[key] = getValue(value, 'name');
            });
        }).then(function(res) {
            return entry;
        });
    };
    
    Addresses.observe('loaded', function logQuery(ctx, next) {
        let helpers = ctx.Model.app.models.helpers;
        preOut(helpers, ctx.data).then(function(v) {
            next();
        });
    });
    
    Addresses.observe('before save', function logQuery(ctx, next) {
        let helpers = ctx.Model.app.models.helpers;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        preSave(helpers, data).then(function(v) {
            next();
        });
    });
};
