'use strict';

var Promise = require("bluebird");
var _ = require("lodash");
var qr = require('qr-image');
var admin = require("firebase-admin");
var fs = require('fs');
const path = require('path');
var YAML = require('json2yaml');

var serviceAccount = require("../../../server/mkv-dev-6102b8d071f4.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://mkv-dev.firebaseio.com/"
});

module.exports = function(Profiles) {
    
    Profiles.disableRemoteMethod('__destroyById__events', false);
    Profiles.disableRemoteMethod('__destroyById__exponents', false);
    Profiles.disableRemoteMethod('__destroyById__contacts', false);
    
    let fields = {
        "positions": "position",
        "sexes": "sex",
        "citizenships": "citizenship"
    };
    
    let stats = {
        "events": "events_count",
        "exponents": "exponents_count",
        "messages": "messages_count",
        "invites": "invites_count",
        "requests": "requests_count",
        "contacts": "contacts_count",
        "profiles": "profiles_count"
    };
    
    Profiles.once('attached', function() {
        var find = Profiles.find;
        Profiles.find = function(filter, cb) {
            if (cb.accessToken != undefined && cb.accessToken.userId != undefined) {
                if (filter == undefined) {
                    arguments[0] = {"where": {"id": {"neq": cb.accessToken.userId}}};
                }
            }
            return find.apply(this, arguments);
        };
    });
    
    var getHelperData = function(helper, type, value, byField = 'id') {
        if (value === "" || value === undefined || value === null) {
            return new Promise.resolve();
        } else {
            if (byField == 'id') {
                let data = {name: value, type: type};
                let create_data = {name: value, type: type, owner_id: 1};
                return helper.findOrCreate({where: data}, create_data);
            } else {
                return helper.findById(value);
            }
        }
    };
    
    var hasPreSave = function(data) {
        let fields_values = _.map(fields, function(v) { return v; });
        let data_keys = Object.keys(data);
        let result = _.intersection(data_keys, fields_values);
        if(result.length > 0) {
            return 1;
        }
    }

    var getValue = function(raw, field = 'id') {
        if (raw == null) {
            return null;
        }
        if (Array.isArray(raw)) {
            return raw[0][field];
        } else {
            return raw[field];
        }
    };
    
    var preSave = function(helpers, entry) {
        return Promise.map(Object.keys(fields), function(field) {
            let key = fields[field];
            return getHelperData(helpers, field, entry[key], 'id').then(
                function(value) {
                    entry[key + "_id"] = getValue(value, 'id');
                }
            );
        }).then(function(res) {
            return entry;
        });
    };
    
    var preOut = function(helpers, entry) {
        return Promise.map(Object.keys(fields), function(field) {
            let key = fields[field];
            return getHelperData(helpers, field, entry[key + "_id"], 'name')
            .then(function(value) {
                entry[key] = getValue(value, 'name');
            });
        }).then(function(res) {
            return entry;
        });
    };
    
    var getStat = function(profile_id, entry) {
        return Profiles.findById(profile_id).then(function(m) {
            return Promise.map(Object.keys(stats), function(field) {
                let key = stats[field];
                if (field == "messages") {
                    let ms = Profiles.app.models.messages;
                    let pc = Profiles.app.models.profiles_chats;
                    return pc.find({"where": {"profile_id": profile_id}}).then(function(value) {
                        let chats_ids = _.map(value, "chat_id");
                        return ms.count({"chat_id": {"inq": chats_ids}}).then(function(value) {
                            entry[key] = value;
                        });
                    }
                );
            } else if (field == "profiles") {
                return Profiles.count().then(
                    function(value) {
                        entry[key] = value;
                    });
                } else {
                    let relate = m[field];
                    return relate.count().then(
                        function(value) {
                            entry[key] = value;
                        });
                    }
                }).then(function(res) {
                    return entry;
                });
            }).then(function(res) {
                return entry;
            }
        );
    };
    
    Profiles.observe('loaded', function logQuery(ctx, next) {
        let helpers = ctx.Model.app.models.helpers;
        preOut(helpers, ctx.data).then(function(v) {
            if (ctx.options.accessToken != null && ctx.data.id == ctx.options.accessToken.userId) {
                getStat(ctx.data.id, ctx.data).then(function(v) {
                    next();
                });
            } else {
                next();
            }
        });
    });
    
    Profiles.observe('before save', function logQuery(ctx, next) {
        let helpers = ctx.Model.app.models.helpers;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        if (!hasPreSave(data)) {
            next();
        } else {
            preSave(helpers, data).then(function(v) {
                next();
            });
        }
    });
    
    Profiles.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static', 'profiles') + '/';
        let dir_exist = fs.existsSync(dirPath);
        if (!dir_exist) {
            let dir = fs.mkdirSync(dirPath);
        }
        data.title = data.first_name + ' ' + data.last_name;
        data.category = "members";
        data.layout = "company-page";
        let ymlText;
        let data_obj = JSON.parse(JSON.stringify(data));
        ymlText = YAML.stringify(data_obj) + "\r\n---";
        fs.writeFile(dirPath + data.id + '.md', ymlText, function(err) {
            if (err) {
                return console.log(err);
            }
        });
        next();
    });
    
    Profiles.addContact = function(profile_id, rel_id, res) {
        console.log('gi', profile_id, 'ii', rel_id);
        let rel_contacts = this.app.models.profiles_contacts;
        rel_contacts.findOrCreate({ contact_id: rel_id, profile_id: profile_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Profiles.deleteContact = function(profile_id, rel_id, res) {
        console.log('gi', profile_id, 'ii', rel_id);
        let rel_contacts = this.app.models.profiles_contacts;
        rel_contacts.destroyAll({ contact_id: rel_id, profile_id: profile_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Profiles.addEvent = function(profile_id, rel_id, res) {
        console.log('gi', profile_id, 'ii', rel_id);
        let rel_events = this.app.models.profiles_events;
        rel_events.findOrCreate({ event_id: rel_id, profile_id: profile_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Profiles.deleteEvent = function(profile_id, rel_id, res) {
        console.log('gi', profile_id, 'ii', rel_id);
        let rel_events = this.app.models.profiles_events;
        rel_events.destroyAll({"and": [{ event_id: rel_id}, {profile_id: profile_id }]}, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Profiles.addExponent = function(profile_id, rel_id, res) {
        console.log('gi', profile_id, 'ii', rel_id);
        let rel_events = this.app.models.profiles_exponents;
        rel_events.findOrCreate({ exponent_id: rel_id, profile_id: profile_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Profiles.deleteExponent = function(profile_id, rel_id, res) {
        console.log('gi', profile_id, 'ii', rel_id);
        let rel_events = this.app.models.profiles_exponents;
        rel_events.destroyAll({ exponent_id: rel_id, profile_id: profile_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    
    Profiles.generateQR = function(profile_id, res) {
        var code = qr.image('{"type":"profiles", "id":' + profile_id + '}', { type: 'png' });
        res.setHeader('Content-type', 'image/png');
        code.pipe(res);
    };
    
    Profiles.refresh = function(refresh_token, res) {
        console.log('rt', refresh_token);
        let rt = this.app.models.refresh_tokens;
        let at = this.app.models.AccessToken;
        rt.find({where:{token: refresh_token}}, function (err, rt_model) {
            if (err) throw err;
            console.log('rt_model', rt_model);
            if (rt_model.length <= 0) {
                return res.json({ "status": false, "data": null });
            }
            var now = Date.now();
            var created = rt_model[0].created_at.getTime();
            var elapsedSeconds = (now - created) / 1000;
            var secondsToLive = rt_model[0].ttl;
            var isValid = elapsedSeconds < secondsToLive;
            /*
            console.log('now', now);
            console.log('created', created);
            console.log('elapsedSeconds', elapsedSeconds);
            console.log('secondsToLive', secondsToLive);
            console.log('isValid', isValid);
            */
            if (!isValid) {
                return res.json({ "status": false, "data": null });
            }
            rt.createRefreshTokenId(function (err, at_uid) {
                if (err) throw err;
                rt.createRefreshTokenId(function (err, rt_uid) {
                    if (err) throw err;
                    at.create({ id: at_uid, userId: rt_model[0].userId }, function (err, at_model) {
                        if (err) throw err;
                        rt.create({ token: rt_uid, userId: rt_model[0].userId }, function (err, rt_new_model) {
                            if (err) throw err;
                            let data = {
                                access_token: at_model.id,
                                access_expire: at_model.ttl,
                                refresh_token: rt_new_model.token,
                                refresh_expire: rt_new_model.ttl
                            };
                            return res.json({ "status": true, "data": data });
                        });
                    });
                });
            });
        });
    };
    
    Profiles.testPush = function(profile_id, res) {
        var payload = {
            notification: {
                title: "новое сообщение",
                body: "Привет че как оно?"
            },
            data: {
                "id": "1",
                "chat_id": "1",
                "message": "Привет че как оно?",
                "owner_id": "1",
                "created_at": "2017-08-18T12:43:48.000Z",
                "updated_at": "2017-08-18T12:43:48.000Z",
                "profile_id": "2"
            }
        };
        Profiles.findById(profile_id).then(function(profile) {
            if (profile.push_token != null) {
                admin.messaging().sendToDevice(profile.push_token, payload)
                .then(function(response) {
                    console.log("Successfully sent message:", response);
                    console.log("Successfully sent message:", response.results);
                    return res.json({ "status": true, "data": true });
                })
                .catch(function(error) {
                    console.log("Error sending message:", error);
                    return res.json({ "status": false, "data": false });
                });
            } else {
                return res.json({ "status": false, "data": "no token" });
            }
        });
    };
    
    Profiles.remoteMethod('generateQR', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/:profile_id/qr', verb: 'get'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('testPush', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/:profile_id/push', verb: 'get'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('refresh', {
        accepts: [
            {
                arg: 'refresh_token',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/refresh', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('addContact', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/:profile_id/contacts/:rel_id', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('deleteContact', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/:profile_id/contacts/remove/:rel_id', verb: 'delete'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('addEvent', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/:profile_id/events/:rel_id', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('deleteEvent', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:profile_id/events/remove/:rel_id', verb: 'delete' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('addExponent', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: {path: '/:profile_id/exponents/:rel_id', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
    Profiles.remoteMethod('deleteExponent', {
        accepts: [
            {
                arg: 'profile_id',
                type: 'string',
                required: true
            },
            {
                arg: 'rel_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:profile_id/exponents/remove/:rel_id', verb: 'delete' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
    
};
