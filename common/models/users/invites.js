'use strict';

module.exports = function(Invites) {
    Invites.once('attached', function() {
        var find = Invites.find;
        var create = Invites.create;
        Invites.create = function(data, cb, t) {
            var self = this;
            var args = arguments;
            this.find({where: {and: [{ owner_id: data.owner_id}, {profile_id: data.profile_id }]}}, function(err, obj){
                if (obj.length == 0) {
                    return create.apply(self, args);
                } else {
                    if (typeof t === 'function') {
                        return t(null, obj[0]);
                    }
                    if (typeof cb === 'function') {
                        return cb(null, obj[0]);
                    }
                }
            });
        };
    });
    Invites.accept = function(invites_id, res) {
        Invites.updateAll({ id: invites_id }, { status: 1 }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    Invites.decline = function(invites_id, res) {
        Invites.updateAll({ id: invites_id }, { status: 2 }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };
    Invites.remoteMethod('accept', {
        accepts: [
            {
                arg: 'invites_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:invites_id/accept', verb: 'post' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
);
Invites.remoteMethod('decline', {
    accepts: [
        {
            arg: 'invites_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
    ],
    http: { path: '/:invites_id/decline', verb: 'post' },
    returns: {
        arg: 'result',
        type: 'string'
    }
}
);
};
