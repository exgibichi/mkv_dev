'use strict';

module.exports = function(chats) {
    chats.observe('loaded', function logQuery(ctx, next) {
        let messages = ctx.Model.app.models.messages;
        messages.find({"order": "id DESC", "limit": 1, "where":{"chat_id": ctx.data.id}}, function(err, message) {
            ctx.data.last_message = message[0];
            next(); 
        });
    });
};
