'use strict';

module.exports = function(Gallery) {
    
    Gallery.addItem = function(gallery_id, item_id, res) {
        console.log('gi', gallery_id, 'ii', item_id);
        let files = this.app.models.files;
        files.updateAll({id: item_id}, {gallery_id: gallery_id}, function(err, info) {
            if (err) throw err;
            return res.json({"status": true, "data": true});
        });
    };
    
    Gallery.deleteItem = function(gallery_id, item_id, res) {
        console.log('gi', gallery_id, 'ii', item_id);
        let files = this.app.models.files;
        files.updateAll({id: item_id}, {gallery_id: null}, function(err, info) {
            if (err) throw err;
            return res.json({"status": true, "data": true});
        });
    };
    
    Gallery.remoteMethod('addItem', {
        accepts: [
        {
            arg: 'gallery_id',
            type: 'string',
            required: true
        },
        {
            arg: 'item_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:gallery_id/items/:item_id', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
    
    Gallery.remoteMethod('deleteItem', {
        accepts: [
        {
            arg: 'gallery_id',
            type: 'string',
            required: true
        },
        {
            arg: 'item_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:gallery_id/items/remove/:item_id', verb: 'delete'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
};
