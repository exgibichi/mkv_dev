'use strict';

module.exports = function(Pages) {
    
    Pages.addGallery = function(page_id, gallery_id, res) {
        let gallery = this.app.models.galleries;
        gallery.updateAll({id: gallery_id}, {page_id: page_id}, function(err, info) {
            if (err) throw err;
            return res.json({"status": true, "data": true});
        });
    };
    
    Pages.deleteGallery = function(page_id, gallery_id, res) {
        let gallery = this.app.models.galleries;
        gallery.updateAll({id: gallery_id}, {page_id: null}, function(err, info) {
            if (err) throw err;
            return res.json({"status": true, "data": true});
        });
    };
    
    Pages.addSEO = function(page_id, seo_id, res) {
        let seo = this.app.models.seo_data;
        seo.updateAll({id: seo_id}, {page_id: page_id}, function(err, info) {
            if (err) throw err;
            console.log('info', info);
            return res.json({"status": true, "data": true});
        });
    };
    
    Pages.deleteSEO = function(page_id, seo_id, res) {
        let seo = this.app.models.seo_data;
        seo.updateAll({id: seo_id}, {page_id: null}, function(err, info) {
            if (err) throw err;
            return res.json({"status": true, "data": true});
        });
    };
    
    Pages.remoteMethod('addGallery', {
        accepts: [
        {
            arg: 'page_id',
            type: 'string',
            required: true
        },
        {
            arg: 'gallery_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:page_id/galleries/:gallery_id', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
    
    Pages.remoteMethod('deleteGallery', {
        accepts: [
        {
            arg: 'page_id',
            type: 'string',
            required: true
        },
        {
            arg: 'gallery_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:page_id/galleries/remove/:gallery_id', verb: 'delete'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );    
    Pages.remoteMethod('addSEO', {
        accepts: [
        {
            arg: 'page_id',
            type: 'string',
            required: true
        },
        {
            arg: 'seo_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:page_id/seo_data/:seo_id', verb: 'post'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
    
    Pages.remoteMethod('deleteSEO', {
        accepts: [
        {
            arg: 'page_id',
            type: 'string',
            required: true
        },
        {
            arg: 'seo_id',
            type: 'string',
            required: true
        },
        {
            arg: 'res',
            type: 'object',
            http: {
                source: 'res'
            }
        }
        ],
        http: {path: '/:page_id/seo_data/remove/:seo_id', verb: 'delete'},
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
};
