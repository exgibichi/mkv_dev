'use strict';

var Promise = require("bluebird");
var _ = require("lodash");

module.exports = function(Contacts) {
    
    let fields = {
        "emails": "emails",
        "phones": "phones",
        "sites": "sites",
        "messengers": "messengers"
    };
    let rels = {
        "sites": [],
        "emails": [],
        "phones": [],
        "messengers": []
    };
 
    var preSave = function (models, entry) {
        return Promise.map(Object.keys(fields), function (field) {
            let key = fields[field];
            let relation;
            if (entry[key] != undefined && typeof entry[key] != "function") {
                relation = entry[key];
            } else if (entry.__cachedRelations != undefined && entry.__cachedRelations[key] != undefined) {
                relation = entry.__cachedRelations[key];
            }
            if(typeof relation === "object" || Array.isArray(relation)) {
                rels[field] = relation;
            }
        }).then(function (res) {
            return entry;
        });
    };
    
    Contacts.observe('before save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        rels = {
            "sites": [],
            "emails": [],
            "phones": [],
            "messengers": []
        };
        preSave(models, data).then(function (v) {
            next();
        });
    });
    Contacts.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let contact_id = ctx.instance.id;
        _.map(Object.keys(rels), function (rel_entry_key) {
            var model = models[rel_entry_key];
            var rel_entries = rels[rel_entry_key];
            _.map(rel_entries, function(rel_entry) {                
                if (rel_entry.id !== undefined) {
                    model.findById(rel_entry.id, function (err, res) {
                        if (err) { throw err; }
                        res.updateAttributes(rel_entry, function (err, res) {
                            if (err) { throw err; }
                            return res;
                        });
                    });
                } else {
                    rel_entry.owner_id = 1;
                    rel_entry.contact_id = contact_id;
                    model.findOrCreate(rel_entry, function (err, res) {
                        if (err) { throw err; }
                        return res;
                    });
                }
            });
        });
        next();
    });
};
