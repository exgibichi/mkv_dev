'use strict';
const { exec } = require('child_process');
const path = require('path');
let dirPath = path.join(__dirname, '..', '..', '..', '..', 'mkv_site') + '/';

module.exports = function(Forum) {
    Forum.hexo = function (passwd, res) {

        if (passwd != 'rgsh') {
            return res.json({ "status": false, "data": false });
        }
        exec("cd " + dirPath + " && hexo generate;", function(err, r) {
            console.log("err", err);
            console.log("r", r);
        });
        return res.json({ "status": true, "data": true });
    };

    Forum.remoteMethod('hexo', {
        accepts: [
            {
                arg: 'passwd',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { verb: "get" },
        returns: {
            arg: 'result',
            type: 'string'
        }
    });
};
