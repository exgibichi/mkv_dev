'use strict';

module.exports = function(Workdays) {

    Workdays.addItem = function(workdays_id, item_id, res) {
        let workhours = this.app.models.workhours;
        workhours.updateAll({ id: item_id }, { workdays_id: workdays_id }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };

    Workdays.deleteItem = function(workdays_id, item_id, res) {
        let workhours = this.app.models.workhours;
        workhours.updateAll({ id: item_id }, { workdays_id: null }, function(err, info) {
            if (err) throw err;
            return res.json({ "status": true, "data": true });
        });
    };

    Workdays.remoteMethod('addItem', {
        accepts: [
            {
                arg: 'workdays_id',
                type: 'string',
                required: true
            },
            {
                arg: 'item_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:workdays_id/workhours/:item_id', verb: 'post' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );

    Workdays.remoteMethod('deleteItem', {
        accepts: [
            {
                arg: 'workdays_id',
                type: 'string',
                required: true
            },
            {
                arg: 'item_id',
                type: 'string',
                required: true
            },
            {
                arg: 'res',
                type: 'object',
                http: {
                    source: 'res'
                }
            }
        ],
        http: { path: '/:workdays_id/workhours/remove/:item_id', verb: 'delete' },
        returns: {
            arg: 'result',
            type: 'string'
        }
    }
    );
};
