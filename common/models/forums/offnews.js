'use strict';

var fs = require('fs');
const path = require('path');
var YAML = require('json2yaml');
//let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static', 'posts') + '/';
let dirPath = path.join(__dirname, '..', '..', '..', '..', 'mkv_site', 'source', '_posts') + '/';

module.exports = function(OffNews) {
    OffNews.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        let dir_exist = fs.existsSync(dirPath);
        if (!dir_exist) {
            let dir = fs.mkdirSync(dirPath);
        }
        OffNews.findById(data.id, function(err, model) {
            model.category = "offnews";
            model.layout = "official-page";
            let ymlText;
            let data_obj = JSON.parse(JSON.stringify(model));
            ymlText = YAML.stringify(data_obj) + "\r\n---";
            fs.writeFile(dirPath + 'offnews_' + model.id + '.md', ymlText, function(err) {
                if (err) {
                    return console.log(err);
                }
            });
        });
        next();
    });
    OffNews.observe('after delete', function (ctx, next) {
        if (ctx.where.id != undefined) {
            fs.unlink(dirPath + 'offnews_' + ctx.where.id + '.md', function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        }
        next();
    });
};
