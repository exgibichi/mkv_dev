'use strict';

var fs = require('fs');
const path = require('path');
var YAML = require('json2yaml');
//let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static', 'posts') + '/';
let dirPath = path.join(__dirname, '..', '..', '..', '..', 'mkv_site', 'source', '_posts') + '/';

module.exports = function(Partners) {
    Partners.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        let dir_exist = fs.existsSync(dirPath);
        if (!dir_exist) {
            let dir = fs.mkdirSync(dirPath);
        }
        Partners.findById(data.id, function (err, model) {
            model.title = data.name;
            model.category = "partners";
            model.layout = "partner-page";
            let ymlText;
            let data_obj = JSON.parse(JSON.stringify(model));
            ymlText = YAML.stringify(data_obj) + "\r\n---";
            fs.writeFile(dirPath + 'partner_' + data.id + '.md', ymlText, function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        });
        next();
    });
    Partners.observe('after delete', function (ctx, next) {
        if (ctx.where.id != undefined) {
            fs.unlink(dirPath + 'partner_' + ctx.where.id + '.md', function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        }
        next();
    });
};
