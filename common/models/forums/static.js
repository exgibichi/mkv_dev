'use strict';

var fs = require('fs');
const path = require('path');

module.exports = function(Static) {
    Static.observe('loaded', function logQuery(ctx, next) {
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        if (data.id != undefined) {
            data.url = "/files/static/" + data.id + ".html";
        }
        next();
    });
    Static.observe('after save', function logQuery(ctx, next) {
        let models = ctx.Model.app.models;
        let data = ctx.instance !== undefined ? ctx.instance : ctx.data;
        let dirPath = path.join(__dirname, '..', '..', '..', 'client', 'files', 'static') + '/';
        let dir_exist = fs.existsSync(dirPath);
        if (!dir_exist) {
            let dir = fs.mkdirSync(dirPath);
        }
        fs.writeFile(dirPath + data.id + '.html', data.html, function(err) {
            if (err) {
                return console.log(err);
            }
        });
        next();
    });
};
