// General Parameters for this app, used during initialization
var AllowTopLevel = false;
var map_settings = {};
var default_cell_size = 50;
var CellSize = new go.Size(default_cell_size, default_cell_size);
var mapObjectsModel;
var server = "localhost:3000";
var api_url_map = "http://" + server + "/api/maps";
var api_url_obj = "http://" + server + "/api/objects";
var api_url_img = "http://" + server + "/api/maps/upload";
var debug = 1;
var removed = {};
var urlToParse = location.search;
var parseQueryString = function(url) {
    var urlParams = {};
    url.replace(
        new RegExp("([^?=&]+)(=([^&]*))?", "g"),
        function($0, $1, $2, $3) {
            urlParams[$1] = $3;
        }
    );

    return urlParams;
}
var map_id = parseQueryString(urlToParse).id;

function init() {
    var $ = go.GraphObject.make;
    map =
    $(go.Diagram, 'map', {
        grid: $(go.Panel, "Grid", {
            gridCellSize: CellSize
        },
        $(go.Shape, "LineH", {
            stroke: "lightgray"
        }),
        $(go.Shape, "LineV", {
            stroke: "lightgray"
        })
    ),
    // support grid snapping when dragging and when resizing
    "draggingTool.isGridSnapEnabled": true,
    "draggingTool.gridSnapCellSpot": go.Spot.Center,
    "resizingTool.isGridSnapEnabled": true,
    allowDrop: true, // handle drag-and-drop from the Palette
    // For this sample, automatically show the state of the diagram's model on the page
    "ModelChanged": function(e) {
        if (e.isTransactionFinished) {
            mapObjectsModel = map.model.toJson();
            mapObjects.model = go.Model.fromJson(mapObjectsModel);
            if (debug) {
                console.log("map object after transaction: ", mapObjectsModel);
            }
        }
    },
    "animationManager.isEnabled": false,
    "undoManager.isEnabled": true
});
map.initialContentAlignment = go.Spot.Center;
map.scrollMode = go.Diagram.InfiniteScroll;
// Regular Nodes represent items to be put onto racks.
// Nodes are currently resizable, but if that is not desired, just set resizable to false.
map.nodeTemplate =
$(go.Node, "Auto", {
    resizable: true,
    resizeObjectName: "SHAPE",
    // because the gridSnapCellSpot is Center, offset the Node's location
    locationSpot: new go.Spot(0, 0, CellSize.width / 2, CellSize.height / 2),
    // provide a visual warning about dropping anything onto an "item"
    mouseDragEnter: function(e, node) {
        e.handled = true;
        node.findObject("SHAPE").fill = "red";
        highlightGroup(node.containingGroup, false);
    },
    mouseDragLeave: function(e, node) {
        node.updateTargetBindings();
    },
    mouseDrop: function(e, node) { // disallow dropping anything onto an "item"
    node.diagram.currentTool.doCancel();
}
},

new go.Binding("position", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
$(go.Shape, "Rectangle", {
    name: "SHAPE",
    fill: "white",
    minSize: CellSize,
    desiredSize: CellSize
},
new go.Binding("fill", "color"),
new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)),
$(go.TextBlock, "Секция", {
    stroke: "#DADADA",
    alignment: go.Spot.TopLeft,
    alignmentFocus: go.Spot.BottomRight,
    font: 'bold 18px sans-serif'
},
new go.Binding("text", "section").makeTwoWay()),
$(go.Panel, 'Vertical', {},
$(go.TextBlock, "Название", {
    stroke: "white",
    alignment: go.Spot.Center,
    font: 'bold 24px sans-serif',
    editable: true
},
new go.Binding("text", "name").makeTwoWay()),
$(go.TextBlock, "Название на анг.", {
    stroke: "#DADADA",
    alignment: go.Spot.Center,
    font: 'bold 18px sans-serif',
    editable: true
},
new go.Binding("text", "name_eng").makeTwoWay())),
$(go.TextBlock, "Локальный номер", {
    background: "white",
    margin: 1,
    font: "bold 14px sans-serif",
    alignment: go.Spot.TopRight,
    alignmentFocus: go.Spot.TopRight
},
new go.Binding("text", "local_num").makeTwoWay()),
$(go.TextBlock, "Глобальный номер", {
    stroke: "white",
    margin: 1,
    font: "bold 14px sans-serif",
    alignment: go.Spot.BottomRight,
    alignmentFocus: go.Spot.BottomRight
},
new go.Binding("text", "global_num").makeTwoWay()), {
    toolTip: // define a tooltip for each node that displays the color as text
    $(go.Adornment, "Auto",
    $(go.Shape, {
        fill: "#FFFFCC"
    }),
    $(go.TextBlock, {
        margin: 4
    },
    new go.Binding("text", "desc"))
)
}, {
    contextMenu: // define a context menu for each node
    $(go.Adornment, "Vertical", // that has one button
    $("ContextMenuButton",
    $(go.TextBlock, "Редактировать"), {
        click: editObject
    }),
    $("ContextMenuButton",
    $(go.TextBlock, "Удалить"), {
        click: removeObject
    })
)
}
);

var mapOverview =
$(go.Overview, 'mapOverviewDiv', {
    observed: map
});

function focusObject(e, node) {
    console.log("node", node);
    console.log("node.data", node.data);
    console.log("mapnode.data", map.model.findNodeDataForKey(node.data.key).pos.split(" "));
    var pos = map.model.findNodeDataForKey(node.data.key).pos.split(" ");
    map.position = new go.Point(parseInt(pos[0]), parseInt(pos[1]));
    console.log("new coord x , y", node.actualBounds.y, node.actualBounds.x);
}

map.nodeListTemplate =
$(go.Node, "Auto", {
    click: focusObject,
    resizable: false,
    resizeObjectName: "SHAPE",
    locationSpot: go.Spot.Center
},
new go.Binding("position", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
$(go.Shape, "Rectangle", {
    name: "SHAPE",
    fill: "white",
    minSize: new go.Size(150, 50),
},
new go.Binding("fill", "color")),
$(go.TextBlock, "Название", {
    stroke: "white",
    alignment: go.Spot.Center,
    font: 'bold 24px sans-serif',
    editable: true
},
new go.Binding("text", "name").makeTwoWay()));

map.contextMenu =
$(go.Adornment, "Vertical",
$("ContextMenuButton",
$(go.TextBlock, "Отменить последнее действие"), {
    click: function(e, obj) {
        e.diagram.commandHandler.undo();
    }
},
new go.Binding("visible", "", function(o) {
    return o.diagram.commandHandler.canUndo();
}).ofObject()),
$("ContextMenuButton",
$(go.TextBlock, "Вернуть последнее действие"), {
    click: function(e, obj) {
        e.diagram.commandHandler.redo();
    }
},
new go.Binding("visible", "", function(o) {
    return o.diagram.commandHandler.canRedo();
}).ofObject()),
$("ContextMenuButton",
$(go.TextBlock, "Создать объект"), {
    click: function(e, obj) {
        jQuery("#add_object_dialog").dialog();
    }
})
);

map.mouseDragOver = function(e) {
    if (!AllowTopLevel) {
        // but OK to drop a group anywhere
        if (!e.diagram.selection.all(function(p) {
            return p instanceof go.Group;
        })) {
            e.diagram.currentCursor = "not-allowed";
        }
    }
};

function addNode(data, is_removed) {
    if(debug) {
        console.log("new object", data);
    }
    data.key += "";
    if(!is_removed && data.key.indexOf('t') == -1) {
        if(debug) {
            console.log("loaded obj", data);
        }
        return;
    }
    data.map_id = map_id;
    jQuery.ajax({
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        url : api_url_obj,
        type : 'POST',
        data: JSON.stringify(data),
        success : function(response, textStatus, jqXhr) {
            if(debug) {
                console.log("add success", response);
            }
            if(response.status) {
                let id = response.data.id;
                let key = data.key;
                let obj = map.model.findNodeDataForKey(key);
                map.startTransaction("edit object");
                map.model.setDataProperty(obj, "key", id+"");
                map.commitTransaction("edit object");
                if(is_removed) {
                    removed[key] = 0;
                }
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            if(debug) {
                console.log("add error: " + textStatus, errorThrown);
            }
        }
    });
}

function removeNode(obj_id) {
    if(debug) {
        console.log("remove object", obj_id);
    }
    if(removed[obj_id] !== undefined && removed[obj_id] === 1) {
        if(debug) {
            console.log("already removed", obj_id);
        }
        let obj = map.model.findNodeDataForKey(obj_id);
        addNode(obj, true);
        return;
    }
    jQuery.ajax({
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        url : api_url_obj + '/' + obj_id,
        type : 'DELETE',
        success : function(response, textStatus, jqXhr) {
            if(debug) {
                console.log("remove success", response);
            }
            removed[obj_id] = 1;
        },
        error : function(jqXHR, textStatus, errorThrown) {
            if(debug) {
                console.log("remove error: " + textStatus, errorThrown);
            }
        }
    });
}

function getKey(data) {
    let is_not_node = data.object === undefined || data.object.key === undefined || data.object.size === undefined || data.object.pos === undefined;
    let is_key = data.propertyName == "key";
    if(is_not_node || is_key) {
        return null;
    }
    if(data.object.key.indexOf('t') !== -1) {
        return null;
    }
    if(debug) {
        console.log("is node", data.object.key);
    }
    return data.object.key;
}

function updateNodes(keys) {
    if(debug) {
        console.log("update nodes", keys);
    }
    jQuery.each(keys, function(i, key) {
        updateNode(key);
    });
}

function updateNode(obj_id) {
    if(debug) {
        console.log("update object", obj_id);
    }
    let obj = map.model.findNodeDataForKey(obj_id);
    if(removed[obj_id] !== undefined && removed[obj_id] === 1) {
        if(debug) {
            console.log("rescue object", obj_id);
        }
        addNode(obj, true);
        return;
    }
    obj.map_id = map_id;
    jQuery.ajax({
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        url : api_url_obj + '/' + obj_id,
        type : 'PATCH',
        data: JSON.stringify(obj),
        success : function(response, textStatus, jqXhr) {
            if(debug) {
                console.log("update success",response);
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            if(debug) {
                console.log("update error: " + textStatus, errorThrown);
            }
        }
    });
}

map.addModelChangedListener(function(evt) {
    if (!evt.isTransactionFinished) return;
    var txn = evt.object;
    if (txn === null) return;
    let counter = txn.changes.length;
    let keys = [];
    txn.changes.each(function(e) {
        if (e.modelChange === "nodeDataArray" && e.change === go.ChangedEvent.Insert) {
            addNode(e.newValue, false);
        } else if (e.modelChange === "nodeDataArray" && e.change === go.ChangedEvent.Remove) {
            removeNode(e.oldValue.key);
        } else if (e.change === go.ChangedEvent.Property) {
            let key = getKey(e);
            if(key !== null && keys.indexOf(key) == -1) {
                keys.push(key);
            }
        }
        counter--;
        if(!counter) {
            updateNodes(keys);
        }
    });
});

jQuery("#accordion").accordion({
    activate: function(event, ui) {
        mapObjects.requestUpdate();
        objectTemplates.requestUpdate();
    }
});

mapObjects =
$(go.Palette, "mapObjects", {
    nodeTemplate: map.nodeListTemplate,
    layout: $(go.GridLayout)
});

var red = '#A50F14';

objectTemplates =
$(go.Palette, "objectTemplates", {
    nodeTemplate: map.nodeTemplate,
    layout: $(go.GridLayout)
});

objectTemplates.model = new go.GraphLinksModel([{
    key: "t",
    color: red,
    size: "150 150"
}]);

var objectKeyId;

function editObject(e, obj) {
    jQuery("#edit_object_dialog").dialog();
    var contextmenu = obj.part;
    var nodedata = contextmenu.data;
    objectKeyId = nodedata.key;
    var inputs = jQuery('#object :input');
    inputs.each(function() {
        jQuery(this).val(nodedata[this.name]);
    });
    if (debug) {
        console.log("obj key ", objectKeyId);
        console.log("obj ", nodedata);
    }
}

function removeObject(e, obj) {
    map.startTransaction("remove object");
    map.model.removeNodeData(obj.part.data)
    map.commitTransaction("remove object");
}

function save() {
    let objects = JSON.parse(mapObjectsModel);
    let map_spec = {
        height: map.documentBounds.size.height,
        width: map.documentBounds.size.width,
        img_height: tileRound(map.documentBounds.size.height),
        img_width: tileRound(map.documentBounds.size.width),
        cell_size: map_settings.cell_size,
        zero_point_x: map.documentBounds.x,
        zero_point_y: map.documentBounds.y
    };
    if (debug) {
        console.log("update map model", map_spec);
        console.log("update objects", objects);
    }
    saveImage(function() {
        updateMap(map_spec, function(){
            jQuery.ajax({
                headers : {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
                },
                url : api_url_map + '/' + map_id + '/compile',
                type : 'GET',
                success : function(response, textStatus, jqXhr) {
                    if(debug) {
                        console.log("Map compiled");
                    }
                    alert("успех");
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    if(debug) {
                        console.log("The following error occured: " + textStatus, errorThrown);
                    }
                }
            });
        });
    });
}

function updateMap(map_spec, callback) {
    jQuery.ajax({
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        url : api_url_map + '/' + map_id,
        type : 'PATCH',
        data : JSON.stringify(map_spec),
        success : function(response, textStatus, jqXhr) {
            if(debug) {
                console.log("Map updated");
            }
            callback();
        },
        error : function(jqXHR, textStatus, errorThrown) {
            if(debug) {
                console.log("The following error occured: " + textStatus, errorThrown);
            }
        }
    });
}

function load() {
    jQuery.get(api_url_map + '/' + map_id, function(response) {
        if(response.status) {
            let data = response.data;
            map_settings = data;
            let full_size = map_settings.max_level - map_settings.min_level;
            full_size = full_size > 0 ? full_size : 1;
            map_settings.full_tile_size = map_settings.tile_size*Math.pow(2, (full_size-1));
            jQuery.get(api_url_map + '/' + map_id + '/objects', function(response) {
                if(response.status) {
                    let data = response.data;
                    jQuery.each(data, function(i, n) {
                        n.key = n.id;
                        delete n.id;
                        map.model.addNodeData(n);
                    });
                }
            });
        }
    });
    if (debug) {
        console.log("load map model", mapObjectsModel);
    }
}

/*
function load() {
mapObjectsModel = localStorage.getItem('mapModel');
if (debug) {
console.log("load map model", mapObjectsModel);
}
if(mapObjectsModel != null) {
map.model = go.Model.fromJson(mapObjectsModel);
mapObjects.model = go.Model.fromJson(mapObjectsModel);
}
}
*/
jQuery('#save_button').click(function() {
    save();
});

jQuery('#load_button').click(function() {
    load();
});

jQuery('#add_object_button').click(function() {
    jQuery("#add_object_dialog").dialog();
});

jQuery('#add_cancel, #edit_cancel').click(function() {
    var id = jQuery(this).prop('id');
    if (id == 'add_cancel') {
        jQuery("#add_object_dialog").dialog("close");
    } else {
        jQuery("#edit_object_dialog").dialog("close");
    }
});

var saveData = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (blob, fileName) {
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());

function cellRound(size) {
    let new_size;
    if(size < 0) {
        koef = -1;
    } else {
        koef = 1;
    }
    new_size = Math.ceil(Math.abs(size)/map_settings.cell_size)*map_settings.cell_size;
    return koef*new_size;
}

function tileRound(size) {
    let new_size;
    if(size < 0) {
        koef = -1;
    } else {
        koef = 1;
    }
    new_size = Math.ceil(Math.abs(size)/map_settings.full_tile_size)*map_settings.full_tile_size;
    return koef*new_size;
}

function saveRender(data, filename, callback) {
    if(debug) {
        console.log("Map render upload");
    }

    var formData = new FormData();

    formData.append("map_id", map_id);
    //var content = '<a id="a"><b id="b">hey!</b></a>'; // the body of the new file...
    //var blob = new Blob([content], { type: "text/xml"});
    formData.append("file", data);
    jQuery.ajax({
        url: api_url_img,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response, textStatus, jqXhr) {
            if(debug) {
                console.log("Map render uploaded", response);
            }
            callback();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(debug) {
                console.log("Map render error: " + textStatus, errorThrown);
            }
        },
    });
}

function saveImage(callback) {
    map.scrollMode = go.Diagram.DocumentScroll;
    map.commandHandler.zoomToFit();
    let height = tileRound(map.documentBounds.size.height);
    let width = tileRound(map.documentBounds.size.width);
    let square = width ? width : height;
    var img = map.makeImageData({
        background: "AntiqueWhite",
        scale: map_settings.scale,
        size: new go.Size(square*map_settings.scale, square*map_settings.scale),
        maxSize: new go.Size(Infinity, Infinity)
    });
    if(debug) {
        console.log({
            background: "AntiqueWhite",
            scale: map_settings.scale,
            size: new go.Size(square*map_settings.scale, square*map_settings.scale),
            maxSize: new go.Size(Infinity, Infinity)
        });
    }
    fetch(img)
    .then(res => res.blob())
    .then(blob => saveRender(blob, 'map.png', callback));
    map.scrollMode = go.Diagram.InfiniteScroll;
}

jQuery('#create_object_button').click(function(e) {
    e.preventDefault();
    if (debug) {
        console.log("create new object");
    }
    var values = {};
    jQuery.each(jQuery('#newObject').serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });
    if (debug) {
        console.log("new object values: ", values);
    }
    map.model.addNodeData(values);
    mapObjects.model.addNodeData(values);
    jQuery("#add_object_dialog").dialog("close");
});

jQuery('#save_object_button').click(function(e) {
    e.preventDefault();
    if (debug) {
        console.log("edit object");
    }
    var values = {};
    var obj = map.model.findNodeDataForKey(objectKeyId);
    map.startTransaction("edit object");
    jQuery.each(jQuery('#object').serializeArray(), function(i, field) {
        if (obj[field.name] != field.value) {
            map.model.setDataProperty(obj, field.name, field.value);
            values[field.name] = {
                old: obj[field.name],
                new: field.value
            };
        }
    });
    if (debug) {
        console.log("new values: ", values);
    }
    map.commitTransaction("edit object");
});

jQuery('#zoom_in').click(function(e) {
    map.commandHandler.increaseZoom();
});

jQuery('#zoom_out').click(function(e) {
    map.commandHandler.decreaseZoom();
});

jQuery('#zoom_fit').click(function(e) {
    map.commandHandler.zoomToFit();
});
jQuery('#add_object_dialog').hide();
jQuery('#edit_object_dialog').hide();

load();
}
