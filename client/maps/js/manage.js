$(document).ready(function() {

    function getList() {
        $.get(api_url, function(response) {
            let data = response.data;
            if(response.status) {
                if(data.length) {
                    let tbl = '';
                    $.each(data, function(i, v) {
                        tbl += '<tr><td>' + v.id + '<td><td>' + v.title + '<td><td><a class="btn" href="construct.html?id=' + v.id + '">Загрузить</a><a class="btn" href="map.html?id=' + v.id + '">Посмотреть</a></td><tr>';
                    });
                    $("tbody").html(tbl);
                }
            }
        });
    }

    $('#add_map_button').click(function() {
        $("#add_map_dialog").dialog();
    });

    $('#create_map_button').click(function(e) {
        e.preventDefault();
        if (debug) {
            console.log("create new map");
        }
        var values = {};
        $.each($('#newObject').serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });
        if (debug) {
            console.log("new object values: ", values);
        }
        $.post(api_url, values, function(data) {
            if(data.status) {
                alert("успех");
                getList();
            }
        })
        $("#add_map_dialog").dialog("close");
    });

    $('#add_map_dialog').hide();

    getList();
});
