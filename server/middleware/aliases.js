'use strict';

var _ = require('lodash');

module.exports = function() {
    return function aliases(req, res, next) {
        let replaces = [
        /forum\/(.*)/gi,
        /system\/(.*)/gi,
        /users\/(.*)/gi,
        /quizes\/(.*)/gi,
        /share\/(.*)/gi,
        /auth\/(.*)/gi
        ];
        let i = 0;
        let c = replaces.length;
        _.each(replaces, function(regexp) {
            i++;
            let re = new RegExp(regexp);
            let result = re.exec(req.url);
            if (result !== null && result[1] !== "") {
                let check = result[1].split('/');
                check = check.filter(function(n) { return n !== ""; }); 
                let first = check[0];
                let id_check = parseInt(first);
                if (isNaN(id_check)) {
                    console.log("old url " + req.url);
                    if (result[1] == "signup") {
                        req.url = "/api/profiles";
                    } else if (result[1] == "logout") {
                        req.url = "/api/profiles/logout";
                    } else if (result[1] == "login") {
                        req.url = "/api/profiles/login";
                    } else if (result[1] == "refresh") {
                        req.url = "/api/profiles/refresh";
                    } else {
                        req.url = "/api/" + result[1];
                    }
                    console.log("new url " + req.url);
                    next();
                    return false;
                }
            }
            if (i == c) {
                next();
                return false;
            }
        });
    };
};
