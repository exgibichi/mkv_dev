'use strict';

var test_data = require('./test_migrate/test_data');
var Promise = require('bluebird');
var use_test_data = false;
var use_migrate = false;
module.exports = function(app) {
    let ds = app.dataSources.mysql;
    var lb_tables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];
    var mkv_tables = [
        'addresses',
        'companies',
        'companies_contacts',
        'companies_addresses',
        'contacts',
        'chats',
        'clusters',
        'categories',
        'emails',
        'exponats',
        'exponents',
        'exponats_features',
        'exponats_types',
        'events',
        'events_profiles',
        'events_companies',
        'files',
        'faqs',
        'forum',
        'features',
        'galleries',
        'helpers',
        'invites',
        'map_objects',
        'messengers',
        'messages',
        'news',
        'offnews',
        'objects_types',
        'organizers',
        'profiles',
        'pages',
        'places',
        'points',
        'phones',
        'partners',
        'settings',
        'seo_data',
        'sites',
        'types',
        'themes',
        'quizes',
        'questions',
        'variants',
        'jobs',
        'workhours',
        'workdays',
        'refresh_tokens',
        'maps',
        'objects'
    ];
/*
    ds.automigrate(['objects', 'maps', 'paths'], function (err) {
        if (err) throw err;
        console.log('1 Loopback tables [' + lb_tables + '] created in ', ds.adapter.name);
    });
 //   */
    if (use_migrate) {
        ds.automigrate(lb_tables, function(err) {
            if (err) throw err;
            console.log('1 Loopback tables [' + lb_tables + '] created in ', ds.adapter.name);
        });
        ds.automigrate(mkv_tables, function(err) {
            if (err) throw err;
            // create admin
            app.models["profiles"].create(
                {
                    "email": "superroot@gmail.com",
                    "first_name": "Админ",
                    "last_name": "Админович",
                    "password": "admin",
                    "is_admin": 1
                }
            );
            console.log('2 MKV tables [' + mkv_tables + '] created in ', ds.adapter.name);
            if (use_test_data) {
                mkv_tables.map(function(v, i) {
                    Promise.each(test_data[v], function(sv, si) {
                        return app.models[v].create(sv);
                    }).then(function() {
                        console.log(v + " test data created");
                    });
                });
            }
        });
    }
};
