'use strict';
var _ = require("lodash");

module.exports = function(app) {
  var remotes = app.remotes();
  remotes.after('**', function(ctx, next) {
    let data = ctx.result ? ctx.result : true;
    if (ctx.methodString == "profiles.find" && ctx.req.accessToken !== null) {
      let profiles_contacts = app.models.profiles_contacts;
      profiles_contacts.find({"where": {"profile_id": ctx.req.accessToken.userId}}).then(function(pcs){
        let contacts_ids = _.map(pcs, "contact_id");
        console.log("ci", contacts_ids);
        data = _.map(data, function(d) {
          if (contacts_ids.indexOf(d.id) != -1) {
            d.is_contacted = 1;
          } else {
            d.is_contacted = 0;
          }
          return d;
        });
        ctx.result = {
          status: true,
          data: data
        };
        next();
      });
    } else if (ctx.methodString == "events.find" && ctx.req.accessToken !== null) {
      let profiles_events = app.models.profiles_events;
      profiles_events.find({"where": {"profile_id": ctx.req.accessToken.userId}}).then(function(pes) {
        let events_ids = _.map(pes, "event_id");
        console.log("evi", events_ids);
        data = _.map(data, function(d) {
          if (events_ids.indexOf(d.id) != -1) {
            d.in_favorites = 1;
          } else {
            d.in_favorites = 0;
          }
          return d;
        });
        ctx.result = {
          status: true,
          data: data
        };
        next();
      });
    } else if ((ctx.methodString === "exponents.find" || ctx.methodString === "exponents.findById") && ctx.req.accessToken !== null) {
      let profiles_exponents = app.models.profiles_exponents;
      console.log("wup");
      profiles_exponents.find({"where": {"profile_id": ctx.req.accessToken.userId}}).then(function(pes){
        let exponents_ids = _.map(pes, "exponent_id");
        console.log("exi", exponents_ids);
        if(Array.isArray(data)) {
          data = _.map(data, function(d) {
            if (d.id != undefined && exponents_ids.indexOf(d.id) != -1) {
              d.in_favorites = 1;
            } else {
              d.in_favorites = 0;
            }
            return d;
          });
        } else {
          if (data.id != undefined && exponents_ids.indexOf(data.id) != -1) {
            data.in_favorites = 1;
          } else {
            data.in_favorites = 0;
          }
        }
        ctx.result = {
          status: true,
          data: data
        };
        next();
      });
    } else if ((ctx.methodString === "exponents.create" || ctx.methodString === "exponents.patchOrCreate") && ctx.req.accessToken !== null) {
      let exponents = app.models.exponents;
      console.log("her");
      exponents.findById(data.id, function(err, data) {
        ctx.result = {
          status: true,
          data: data
        };
        next();
      });
    } else if (ctx.methodString === "profiles.login") {
      let rt = app.models.refresh_tokens;
      rt.createRefreshTokenId(function(err, uid) {
        if(err) throw err;
        rt.create({token: uid, userId: data.userId}, function(err, model){
          if (err) throw err;
          data.refresh_token = uid;
          data.refresh_token_expire = model.ttl;
          ctx.result = {
            status: true,
            data: data
          };
          next();
        });
      });
    } else {
      ctx.result = {
        status: true,
        data: data
      };
      next();
    }
  });
  remotes.before('**', function(ctx, next) {
    if (ctx.method.name == 'create' && ctx.req.accessToken !== null) {
      ctx.args.data.owner_id = ctx.req.accessToken.userId;
    }
    next();
  });
};
