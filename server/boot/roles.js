'use strict';

module.exports = function(app) {
    var Role = app.models.Role;
    Role.registerResolver('admins', function(role, context, cb) {
        function reject() {
            process.nextTick(function() {
                cb(null, false);
            });
        }
        var userId = context.accessToken.userId;
        if (!userId) {
            return reject();
        }
        var profile = app.models.profiles;
        profile.count({
            id: userId,
            is_admin: 1
        }, function(err, count) {
            if (err) {
                console.log(err);
                return cb(null, false);
            }
            cb(null, count > 0); // true = is a team member
        });
    });
    Role.registerResolver('participants', function(role, context, cb) {
        function reject() {
            process.nextTick(function() {
                cb(null, false);
            });
        }
        var userId = context.accessToken.userId;
        if (!userId) {
            return reject();
        }
        var profile = app.models.profiles;
        profile.count({
            id: userId,
            is_participant: 1
        }, function(err, count) {
            if (err) {
                console.log(err);
                return cb(null, false);
            }
            cb(null, count > 0); // true = is a team member
        });
    });
};
