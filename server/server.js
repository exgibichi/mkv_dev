'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require('path');

var app = module.exports = loopback();

app.use(loopback.token({
  model: app.models.accessToken,
  currentUserLiteral: 'me'
}));

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Authorization,Content-Type,*");
  res.header("Access-Control-Allow-Methods", "POST,PATCH,GET,OPTIONS,DELETE,*");
  if (req.method.toLowerCase() == 'options') {
    console.log('options');
    res.send('');
  } else {
    next();
  }
});

boot(app, __dirname, function(err) {
  if (err) throw err;
  if (require.main === module)
  app.start();
});
